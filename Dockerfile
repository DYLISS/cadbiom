from python:2.7.13-jessie

RUN apt-get update && apt-get install -y \
        python2.7-dev \
        libxml2-dev \
        libxslt1-dev \
        pkg-config \
	build-essential \
	python-pip

COPY . /cadbiom
WORKDIR /cadbiom

RUN cd /cadbiom/library && make install
RUN cd /cadbiom/command_line && make install

WORKDIR /root
RUN rm -rf /cadbiom

CMD ["bash"]
