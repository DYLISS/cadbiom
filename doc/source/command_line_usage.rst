******************
Command line usage
******************

.. argparse::
   :filename: ../command_line/cadbiom_cmd/__main__.py
   :func: main
   :prog: cadbiom_cmd
