*******
License
*******

Cadbiom is distributed with the GNU General Public License.

::

  CADBIOM (Computer Aided Design of Biological Models) is an open source
  modelling software for cell signalling networks.
  Copyright (C) 2009-2020  IRISA/IRSET/INRIA

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
