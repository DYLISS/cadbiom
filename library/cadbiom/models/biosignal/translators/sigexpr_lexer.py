# Generated from cadbiom/models/biosignal/translators/sigexpr_lexer.g by ANTLR 4.8
# encoding: utf-8
from __future__ import print_function
from antlr4 import *
from io import StringIO
import sys



def serializedATN():
    with StringIO() as buf:
        buf.write(u"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2")
        buf.write(u"!\u00d5\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4")
        buf.write(u"\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r")
        buf.write(u"\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22")
        buf.write(u"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4")
        buf.write(u"\30\t\30\4\31\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35")
        buf.write(u"\t\35\4\36\t\36\4\37\t\37\4 \t \4!\t!\4\"\t\"\3\2\3\2")
        buf.write(u"\3\2\3\2\3\3\3\3\3\3\3\3\7\3N\n\3\f\3\16\3Q\13\3\3\3")
        buf.write(u"\3\3\3\3\3\3\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\5\3\5")
        buf.write(u"\3\5\3\5\3\5\3\6\3\6\3\6\3\6\3\6\3\6\3\7\3\7\3\7\3\7")
        buf.write(u"\3\7\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\t\3\t")
        buf.write(u"\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\n\3\n\3\n\3\n\3\n\3\n")
        buf.write(u"\3\n\3\n\3\n\3\13\3\13\3\13\3\13\3\f\3\f\3\f\3\r\3\r")
        buf.write(u"\3\r\3\r\3\16\3\16\3\16\3\16\3\16\3\17\3\17\3\17\3\17")
        buf.write(u"\3\17\3\17\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3")
        buf.write(u"\20\3\20\3\20\3\21\3\21\3\21\3\22\3\22\3\23\3\23\3\23")
        buf.write(u"\3\24\3\24\3\25\3\25\3\26\3\26\3\27\3\27\3\30\3\30\3")
        buf.write(u"\31\3\31\3\32\3\32\3\33\3\33\3\34\3\34\3\35\3\35\3\36")
        buf.write(u"\3\36\3\37\3\37\3 \3 \3!\3!\3\"\3\"\6\"\u00d2\n\"\r\"")
        buf.write(u"\16\"\u00d3\2\2#\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23")
        buf.write(u"\13\25\f\27\r\31\16\33\17\35\20\37\21!\22#\23%\24\'\25")
        buf.write(u")\26+\27-\30/\31\61\32\63\33\65\34\67\359\36;\37= ?\2")
        buf.write(u"A\2C!\3\2\5\4\2\13\f\"\"\3\2\f\f\5\2C\\aac|\2\u00d5\2")
        buf.write(u"\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3")
        buf.write(u"\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2")
        buf.write(u"\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2")
        buf.write(u"\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2")
        buf.write(u"\2%\3\2\2\2\2\'\3\2\2\2\2)\3\2\2\2\2+\3\2\2\2\2-\3\2")
        buf.write(u"\2\2\2/\3\2\2\2\2\61\3\2\2\2\2\63\3\2\2\2\2\65\3\2\2")
        buf.write(u"\2\2\67\3\2\2\2\29\3\2\2\2\2;\3\2\2\2\2=\3\2\2\2\2C\3")
        buf.write(u"\2\2\2\3E\3\2\2\2\5I\3\2\2\2\7V\3\2\2\2\t^\3\2\2\2\13")
        buf.write(u"c\3\2\2\2\ri\3\2\2\2\17q\3\2\2\2\21x\3\2\2\2\23\u0081")
        buf.write(u"\3\2\2\2\25\u008a\3\2\2\2\27\u008e\3\2\2\2\31\u0091\3")
        buf.write(u"\2\2\2\33\u0095\3\2\2\2\35\u009a\3\2\2\2\37\u00a0\3\2")
        buf.write(u"\2\2!\u00ab\3\2\2\2#\u00ae\3\2\2\2%\u00b0\3\2\2\2\'\u00b3")
        buf.write(u"\3\2\2\2)\u00b5\3\2\2\2+\u00b7\3\2\2\2-\u00b9\3\2\2\2")
        buf.write(u"/\u00bb\3\2\2\2\61\u00bd\3\2\2\2\63\u00bf\3\2\2\2\65")
        buf.write(u"\u00c1\3\2\2\2\67\u00c3\3\2\2\29\u00c5\3\2\2\2;\u00c7")
        buf.write(u"\3\2\2\2=\u00c9\3\2\2\2?\u00cb\3\2\2\2A\u00cd\3\2\2\2")
        buf.write(u"C\u00d1\3\2\2\2EF\t\2\2\2FG\3\2\2\2GH\b\2\2\2H\4\3\2")
        buf.write(u"\2\2IJ\7\61\2\2JK\7\61\2\2KO\3\2\2\2LN\n\3\2\2ML\3\2")
        buf.write(u"\2\2NQ\3\2\2\2OM\3\2\2\2OP\3\2\2\2PR\3\2\2\2QO\3\2\2")
        buf.write(u"\2RS\7\f\2\2ST\3\2\2\2TU\b\3\2\2U\6\3\2\2\2VW\7f\2\2")
        buf.write(u"WX\7g\2\2XY\7h\2\2YZ\7c\2\2Z[\7w\2\2[\\\7n\2\2\\]\7v")
        buf.write(u"\2\2]\b\3\2\2\2^_\7y\2\2_`\7j\2\2`a\7g\2\2ab\7p\2\2b")
        buf.write(u"\n\3\2\2\2cd\7g\2\2de\7x\2\2ef\7g\2\2fg\7p\2\2gh\7v\2")
        buf.write(u"\2h\f\3\2\2\2ij\7u\2\2jk\7{\2\2kl\7p\2\2lm\7e\2\2mn\7")
        buf.write(u"j\2\2no\7t\2\2op\7q\2\2p\16\3\2\2\2qr\7g\2\2rs\7z\2\2")
        buf.write(u"st\7e\2\2tu\7n\2\2uv\7w\2\2vw\7u\2\2w\20\3\2\2\2xy\7")
        buf.write(u"k\2\2yz\7p\2\2z{\7e\2\2{|\7n\2\2|}\7w\2\2}~\7f\2\2~\177")
        buf.write(u"\7g\2\2\177\u0080\7f\2\2\u0080\22\3\2\2\2\u0081\u0082")
        buf.write(u"\7u\2\2\u0082\u0083\7g\2\2\u0083\u0084\7s\2\2\u0084\u0085")
        buf.write(u"\7w\2\2\u0085\u0086\7g\2\2\u0086\u0087\7p\2\2\u0087\u0088")
        buf.write(u"\7e\2\2\u0088\u0089\7g\2\2\u0089\24\3\2\2\2\u008a\u008b")
        buf.write(u"\7c\2\2\u008b\u008c\7p\2\2\u008c\u008d\7f\2\2\u008d\26")
        buf.write(u"\3\2\2\2\u008e\u008f\7q\2\2\u008f\u0090\7t\2\2\u0090")
        buf.write(u"\30\3\2\2\2\u0091\u0092\7p\2\2\u0092\u0093\7q\2\2\u0093")
        buf.write(u"\u0094\7v\2\2\u0094\32\3\2\2\2\u0095\u0096\7v\2\2\u0096")
        buf.write(u"\u0097\7t\2\2\u0097\u0098\7w\2\2\u0098\u0099\7g\2\2\u0099")
        buf.write(u"\34\3\2\2\2\u009a\u009b\7h\2\2\u009b\u009c\7c\2\2\u009c")
        buf.write(u"\u009d\7n\2\2\u009d\u009e\7u\2\2\u009e\u009f\7g\2\2\u009f")
        buf.write(u"\36\3\2\2\2\u00a0\u00a1\7e\2\2\u00a1\u00a2\7q\2\2\u00a2")
        buf.write(u"\u00a3\7p\2\2\u00a3\u00a4\7u\2\2\u00a4\u00a5\7v\2\2\u00a5")
        buf.write(u"\u00a6\7t\2\2\u00a6\u00a7\7c\2\2\u00a7\u00a8\7k\2\2\u00a8")
        buf.write(u"\u00a9\7p\2\2\u00a9\u00aa\7v\2\2\u00aa \3\2\2\2\u00ab")
        buf.write(u"\u00ac\7<\2\2\u00ac\u00ad\7?\2\2\u00ad\"\3\2\2\2\u00ae")
        buf.write(u"\u00af\7?\2\2\u00af$\3\2\2\2\u00b0\u00b1\7#\2\2\u00b1")
        buf.write(u"\u00b2\7?\2\2\u00b2&\3\2\2\2\u00b3\u00b4\7*\2\2\u00b4")
        buf.write(u"(\3\2\2\2\u00b5\u00b6\7+\2\2\u00b6*\3\2\2\2\u00b7\u00b8")
        buf.write(u"\7&\2\2\u00b8,\3\2\2\2\u00b9\u00ba\7.\2\2\u00ba.\3\2")
        buf.write(u"\2\2\u00bb\u00bc\7@\2\2\u00bc\60\3\2\2\2\u00bd\u00be")
        buf.write(u"\7>\2\2\u00be\62\3\2\2\2\u00bf\u00c0\7#\2\2\u00c0\64")
        buf.write(u"\3\2\2\2\u00c1\u00c2\7=\2\2\u00c2\66\3\2\2\2\u00c3\u00c4")
        buf.write(u"\7-\2\2\u00c48\3\2\2\2\u00c5\u00c6\7/\2\2\u00c6:\3\2")
        buf.write(u"\2\2\u00c7\u00c8\7,\2\2\u00c8<\3\2\2\2\u00c9\u00ca\7")
        buf.write(u"`\2\2\u00ca>\3\2\2\2\u00cb\u00cc\t\4\2\2\u00cc@\3\2\2")
        buf.write(u"\2\u00cd\u00ce\4\62;\2\u00ceB\3\2\2\2\u00cf\u00d2\5?")
        buf.write(u" \2\u00d0\u00d2\5A!\2\u00d1\u00cf\3\2\2\2\u00d1\u00d0")
        buf.write(u"\3\2\2\2\u00d2\u00d3\3\2\2\2\u00d3\u00d1\3\2\2\2\u00d3")
        buf.write(u"\u00d4\3\2\2\2\u00d4D\3\2\2\2\6\2O\u00d1\u00d3\3\2\3")
        buf.write(u"\2")
        return buf.getvalue()


class sigexpr_lexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    WS = 1
    COMMENT = 2
    DEFAULT = 3
    WHEN = 4
    EVENT = 5
    SYNC = 6
    EXC = 7
    INC = 8
    SEQ = 9
    AND = 10
    OR = 11
    NOT = 12
    T = 13
    F = 14
    CONSTR = 15
    DEF = 16
    EG = 17
    NOTEG = 18
    PG = 19
    PD = 20
    DOL = 21
    COM = 22
    UP = 23
    DOWN = 24
    CHG = 25
    SCOL = 26
    PLUS = 27
    MINUS = 28
    MUL = 29
    EXP = 30
    IDENT = 31

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ u"DEFAULT_MODE" ]

    literalNames = [ u"<INVALID>",
            u"'default'", u"'when'", u"'event'", u"'synchro'", u"'exclus'", 
            u"'included'", u"'sequence'", u"'and'", u"'or'", u"'not'", u"'true'", 
            u"'false'", u"'constraint'", u"':='", u"'='", u"'!='", u"'('", 
            u"')'", u"'$'", u"','", u"'>'", u"'<'", u"'!'", u"';'", u"'+'", 
            u"'-'", u"'*'", u"'^'" ]

    symbolicNames = [ u"<INVALID>",
            u"WS", u"COMMENT", u"DEFAULT", u"WHEN", u"EVENT", u"SYNC", u"EXC", 
            u"INC", u"SEQ", u"AND", u"OR", u"NOT", u"T", u"F", u"CONSTR", 
            u"DEF", u"EG", u"NOTEG", u"PG", u"PD", u"DOL", u"COM", u"UP", 
            u"DOWN", u"CHG", u"SCOL", u"PLUS", u"MINUS", u"MUL", u"EXP", 
            u"IDENT" ]

    ruleNames = [ u"WS", u"COMMENT", u"DEFAULT", u"WHEN", u"EVENT", u"SYNC", 
                  u"EXC", u"INC", u"SEQ", u"AND", u"OR", u"NOT", u"T", u"F", 
                  u"CONSTR", u"DEF", u"EG", u"NOTEG", u"PG", u"PD", u"DOL", 
                  u"COM", u"UP", u"DOWN", u"CHG", u"SCOL", u"PLUS", u"MINUS", 
                  u"MUL", u"EXP", u"LETTER", u"DIGIT", u"IDENT" ]

    grammarFileName = u"sigexpr_lexer.g"

    def __init__(self, input=None, output=sys.stdout):
        super(sigexpr_lexer, self).__init__(input, output=output)
        self.checkVersion("4.8")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


