## Filename    : gt_visitors
## Author(s)   : Michel Le Borgne
## Created     : 01/2012
## Revision    :
## Source      :
##
## Copyright 2010 - 2020 IRISA/IRSET
##
## This library is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published
## by the Free Software Foundation; either version 2.1 of the License, or
## any later version.
##
## This library is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY, WITHOUT EVEN THE IMPLIED WARRANTY OF
## MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.  The software and
## documentation provided here under is on an "as is" basis, and IRISA has
## no obligations to provide maintenance, support, updates, enhancements
## or modifications.
## In no event shall IRISA be liable to any party for direct, indirect,
## special, incidental or consequential damages, including lost profits,
## arising out of the use of this software and its documentation, even if
## IRISA have been advised of the possibility of such damage.  See
## the GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this library; if not, write to the Free Software Foundation,
## Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
##
## The original code contained here was initially developed by:
##
##     Michel Le Borgne.
##     IRISA
##     Symbiose team
##     IRISA  Campus de Beaulieu
##     35042 RENNES Cedex, FRANCE
##
##
## Contributor(s): Geoffroy Andrieux, Nolwenn Le Meur, Pierre Vignet
##
"""
Utilities for compiling conditions, events and constraints
"""
from antlr4 import InputStream, CommonTokenStream, RecognitionException

try:
    from functools import lru_cache
except ImportError:
    from functools32 import lru_cache

from cadbiom.models.biosignal.translators.sigexpr_lexer import sigexpr_lexer
from cadbiom.models.biosignal.translators.sigexpr_compiler import sigexpr_compiler
from cadbiom.models.biosignal.sig_expr import (
    SigIdentExpr, SigDefaultExpr, SigWhenExpr, SigConstExpr
)
import cadbiom.commons as cm

LOGGER = cm.logger()

import sys

# Increase recursion depth limit for big models with long logical formulas in
# events and conditions.
# Yes it's shit, but as long as the grammar is not fixed to correctly manage
# the associativity of propositional formulas in the definitions of events, this
# code must remain in place to manage the huge syntactic tree of some models.
# => see get_conditions_from_event()
sys.setrecursionlimit(40000)


def compile_cond(text, symb_table, error_reporter, deep=-1, message=""):
    """Compile a condition expression to a tree representation

    :param text: string to be compiled
    :param symb_table: symbol table of the compiler.
        See :class:`cadbiom.models.clause_constraints.CLDynSys.CLDynSys`
    :param error_reporter: use a CompilReporter for full information!!
    :key deep: (optional) if > 0 the compiler doesn't accept states
        declared inside macro. Default: -1
    :key message: Parser message ?
    :type text: <str>
    :type symb_table: <dict <str>: <tuple <str>, <int>>>
    :type error_reporter: <Reporter>
    :type deep: <int>
    :type message: <str>
    :return: a tree representing the expression
    """
    text_c = text + " $"
    reader = InputStream(text_c)
    lexer = sigexpr_lexer(reader)
    parser = sigexpr_compiler(CommonTokenStream(lexer))
    parser.deep = deep
    parser.message = message
    if error_reporter:
        # default options (state_only = False)
        parser.set_error_reporter(error_reporter)
    # ident checking option: only state and input names are allowed
    parser.state_only = True
    try:
        tree = parser.sig_bool(symb_table)
    except RecognitionException as exc:
        if error_reporter:
            error_reporter.display("Error in condition: %s" % exc)
            parser.displayExceptionMessage(exc)
            return None
        else:
            raise exc
    return tree.exp


def compile_constraint(const_txt, symb_table, error_reporter):
    """Compile a constraint expression to a tree representation

    :param const_txt: string to be compiled
    :param symb_table: symbol table of the compiler.
        See :class:`cadbiom.models.clause_constraints.CLDynSys.CLDynSys`
    :param error_reporter: Reporter to output error messages
    :type const_txt: <str>
    :type symb_table: <dict <str>: <tuple <str>, <int>>>
    :type error_reporter: <Reporter>
    :return: a tree representing the expression
    """
    text_c = const_txt + " $"
    reader = InputStream(text_c)
    lexer = sigexpr_lexer(reader)
    parser = sigexpr_compiler(CommonTokenStream(lexer))
    parser.set_error_reporter(error_reporter)
    try:
        tree = parser.sig_expression(symb_table)
    except RecognitionException as exc:
        error_reporter.display("Error in constraints: %s" % exc)
        parser.displayExceptionMessage(exc)
        return None
    return tree.exp


def compile_event(text, symb_table, free_clocks, error_reporter):
    """Compile an event expression to a tree representation

    :param text: string to be compiled
    :param symb_table: symbol table of the compiler.
        See :class:`cadbiom.models.clause_constraints.CLDynSys.CLDynSys`
    :param free_clocks: boolean if true free clocks are collected
    :param error_reporter: Reporter to output error messages
    :type text: <str>
    :type symb_table: <dict <str>: <tuple <str>, <int>>>
    :type free_clocks: <boolean>
    :type error_reporter: <Reporter>
    :return: a tuple (exp, se, fc)
        where exp is the event expression,
        se the state events (s#> ..) used and
        fc is the free clocks used in the event expression.
    :rtype: <tuple>
    """
    text_c = text + " $"
    reader = InputStream(text_c)
    lexer = sigexpr_lexer(reader)
    # default options on compiler (state_only = False)
    parser = sigexpr_compiler(CommonTokenStream(lexer))
    parser.set_error_reporter(error_reporter)
    parser.catch_free_clocks = free_clocks
    # ident checking option: only state and input names are allowed in bool op
    parser.state_only_in_bool = True
    try:
        tree = parser.sig_expression(symb_table)
    except RecognitionException as exc:
        error_reporter.display("Error in event: %s" % exc)
        parser.displayExceptionMessage(exc)
        return None
    return tree.exp, parser.state_events, parser.free_clocks

################################################################################

class Reporter(object):
    """Error reporter.

    Used in CTransitions from ChartModel in order to quickly parse events
    without having to define TableVisitor.

    .. note:: Link the lexer to the model allows to avoid some errors in Reporter.
        We mask error like: "-> dec -> Undeclared event or state".
        This error is generated for every place in a condition/event and should
        only mean that an item has never been met via TableVisitor.
        In practice this is time consuming and useless for what we want to do.
        See parse_condition()
    """

    def __init__(self):
        self.error = False
        self.mess = ""

    def display(self, error_msg):
        """Display the error in the logger"""
        self.error = True

        if "Undeclared event or state" in error_msg:
            return

        LOGGER.error("Event or condition parser:: %s", error_msg)


@lru_cache(maxsize=None)
def get_conditions_from_event(event, symb_table=dict(), error_reporter=Reporter()):
    """Decompile logical formula in event's name and return separated conditions

    It's a high level function! Yes, It could just return the syntaxic tree for
    each event instead of the string version of the conditions. But for now
    we only need strings. Later it could be better to return expressions and
    use an independent version of the internal :meth:`filter_SigExpression` function.

    .. seealso:: :meth:`cadbiom.models.biosignal.sig_expr` module.

    .. note:: This function is dependent of a high value set in sys.setrecursionlimit.
        See the remark at the top of this module.

    :param: Event string.
    :param symb_table: symbol table of the compiler.
        See :class:`cadbiom.models.clause_constraints.CLDynSys.CLDynSys`
        Can be empty dict
    :param error_reporter: Reporter to output error messages
    :type event: <str>
    :type symb_table: <dict <str>: <tuple <str>, <int>>>
    :type error_reporter: <Reporter>
    :return: A dict of events as keys and their conditions as values.
    :rtype: <dict>
        keys: event's names; values: logical formula attached (condition)
    """

    def get_expressions_from_tree(tree):
        """Get list of expressions from the given tree

        The root tree should be a complex tree (``SigDefaultExpr``) and leafs
        can be simple expressions without conditions (``SigConstExpr`` or ``SigIdentExpr``).

        Example:
            ``((_h_xxx) when (cond)) default (_h_yyy)``

        Here ``((_h_xxx) when (cond))`` is a ``SigWhenExpr`` and ``(_h_yyy)`` is
        a ``SigIdentExpr``.
        This function will return ``[SigWhenExpr, SigIdentExpr]``.

        :return: List of expressions
        :rtype: <list>
        """
        if isinstance(tree, SigDefaultExpr):
            return get_expressions_from_tree(tree.left_h) + \
                get_expressions_from_tree(tree.right_h)

        # Here, some trees are belonging to classes SigConstExpr or SigIdentExpr
        # Ex: for the clock "_h_5231":
        #    ... default (_h_5231)" => No condition for this event
        return [tree]


    def filter_SigExpression(expr):
        """Get string version of every conditions associated to their clocks names

        An expression should be a ``SigWhenExpr`` or a ``SigIdentExpr``.

        .. note:: No ``SigConstExpr`` here => should be filtered previously
            by checking null events (``event=""``) in the model.

        :return: Tuples of events names and condition string
        """

        if isinstance(expr, SigWhenExpr):
            # right : SigSyncBinExpr (logical formula), BUT
            # sometimes SigConstExpr (just a True boolean) when clock is empty
            # Ex: "when ()"
            # So, we replace this boolean with an empty condition
            right = "" if isinstance(expr.right_h, SigConstExpr) else str(expr.right_h)
            return expr.left_h.name, right

        if isinstance(expr, SigIdentExpr):
            return expr.name, ""

        raise AssertionError(
            "You should never have been there ! "
            "The expression type <%s> <%s> is not (yet) supported in events..." % (type(expr), expr)
        )


    LOGGER.debug("parse_event: %s", event)

    # Get tree object from event string
    event_sexpr = compile_event(event, symb_table, True, error_reporter)[0]

    # Filter when events
    # Dict: {event_name: event_cond}
    events_conditions = dict(
        filter_SigExpression(expr) for expr in get_expressions_from_tree(event_sexpr)
    )

    LOGGER.debug("Clocks from event parsing: %s", events_conditions)

    return events_conditions
