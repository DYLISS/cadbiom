# Generated from cadbiom/models/biosignal/translators/sigexpr_compiler.g by ANTLR 4.8
# encoding: utf-8
from __future__ import print_function
from antlr4 import *
from io import StringIO
import sys


from cadbiom.models.biosignal.sig_expr import *


def serializedATN():
    with StringIO() as buf:
        buf.write(u"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3")
        buf.write(u"!\u0098\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t")
        buf.write(u"\7\4\b\t\b\4\t\t\t\4\n\t\n\3\2\3\2\3\2\3\2\3\2\3\2\3")
        buf.write(u"\2\3\2\5\2\35\n\2\3\3\3\3\3\3\3\3\3\3\3\3\7\3%\n\3\f")
        buf.write(u"\3\16\3(\13\3\3\3\5\3+\n\3\3\4\3\4\3\4\3\4\3\4\3\4\3")
        buf.write(u"\4\7\4\64\n\4\f\4\16\4\67\13\4\3\5\3\5\3\5\3\5\3\5\3")
        buf.write(u"\5\7\5?\n\5\f\5\16\5B\13\5\3\6\3\6\3\6\3\6\3\6\3\6\7")
        buf.write(u"\6J\n\6\f\6\16\6M\13\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3")
        buf.write(u"\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3")
        buf.write(u"\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\5\7o\n")
        buf.write(u"\7\3\b\3\b\3\b\3\b\5\bu\n\b\3\t\3\t\3\t\3\t\3\t\3\t\3")
        buf.write(u"\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3")
        buf.write(u"\t\5\t\u008b\n\t\3\n\3\n\3\n\3\n\3\n\3\n\7\n\u0093\n")
        buf.write(u"\n\f\n\16\n\u0096\13\n\3\n\2\2\13\2\4\6\b\n\f\16\20\22")
        buf.write(u"\2\2\2\u009f\2\34\3\2\2\2\4*\3\2\2\2\6,\3\2\2\2\b8\3")
        buf.write(u"\2\2\2\nC\3\2\2\2\fn\3\2\2\2\16t\3\2\2\2\20\u008a\3\2")
        buf.write(u"\2\2\22\u008c\3\2\2\2\24\25\5\4\3\2\25\26\7\27\2\2\26")
        buf.write(u"\27\b\2\1\2\27\35\3\2\2\2\30\31\5\20\t\2\31\32\7\27\2")
        buf.write(u"\2\32\33\b\2\1\2\33\35\3\2\2\2\34\24\3\2\2\2\34\30\3")
        buf.write(u"\2\2\2\35\3\3\2\2\2\36\37\5\6\4\2\37&\b\3\1\2 !\7\5\2")
        buf.write(u"\2!\"\5\6\4\2\"#\b\3\1\2#%\3\2\2\2$ \3\2\2\2%(\3\2\2")
        buf.write(u"\2&$\3\2\2\2&\'\3\2\2\2\'+\3\2\2\2(&\3\2\2\2)+\b\3\1")
        buf.write(u"\2*\36\3\2\2\2*)\3\2\2\2+\5\3\2\2\2,-\5\b\5\2-\65\b\4")
        buf.write(u"\1\2./\7\6\2\2/\60\b\4\1\2\60\61\5\b\5\2\61\62\b\4\1")
        buf.write(u"\2\62\64\3\2\2\2\63.\3\2\2\2\64\67\3\2\2\2\65\63\3\2")
        buf.write(u"\2\2\65\66\3\2\2\2\66\7\3\2\2\2\67\65\3\2\2\289\5\n\6")
        buf.write(u"\29@\b\5\1\2:;\7\r\2\2;<\5\n\6\2<=\b\5\1\2=?\3\2\2\2")
        buf.write(u">:\3\2\2\2?B\3\2\2\2@>\3\2\2\2@A\3\2\2\2A\t\3\2\2\2B")
        buf.write(u"@\3\2\2\2CD\5\f\7\2DK\b\6\1\2EF\7\f\2\2FG\5\f\7\2GH\b")
        buf.write(u"\6\1\2HJ\3\2\2\2IE\3\2\2\2JM\3\2\2\2KI\3\2\2\2KL\3\2")
        buf.write(u"\2\2L\13\3\2\2\2MK\3\2\2\2NO\7\16\2\2OP\5\f\7\2PQ\b\7")
        buf.write(u"\1\2Qo\3\2\2\2RS\5\16\b\2ST\b\7\1\2To\3\2\2\2UV\7\7\2")
        buf.write(u"\2VW\7\25\2\2WX\5\4\3\2XY\7\26\2\2YZ\b\7\1\2Zo\3\2\2")
        buf.write(u"\2[\\\7\6\2\2\\]\7\25\2\2]^\5\4\3\2^_\7\26\2\2_`\b\7")
        buf.write(u"\1\2`o\3\2\2\2ab\7!\2\2bc\7\31\2\2co\b\7\1\2de\7!\2\2")
        buf.write(u"ef\7\32\2\2fo\b\7\1\2gh\7!\2\2ho\b\7\1\2ij\7\25\2\2j")
        buf.write(u"k\5\4\3\2kl\7\26\2\2lm\b\7\1\2mo\3\2\2\2nN\3\2\2\2nR")
        buf.write(u"\3\2\2\2nU\3\2\2\2n[\3\2\2\2na\3\2\2\2nd\3\2\2\2ng\3")
        buf.write(u"\2\2\2ni\3\2\2\2o\r\3\2\2\2pq\7\17\2\2qu\b\b\1\2rs\7")
        buf.write(u"\20\2\2su\b\b\1\2tp\3\2\2\2tr\3\2\2\2u\17\3\2\2\2vw\7")
        buf.write(u"\b\2\2wx\7\25\2\2xy\5\22\n\2yz\7\26\2\2z{\b\t\1\2{\u008b")
        buf.write(u"\3\2\2\2|}\7\t\2\2}~\7\25\2\2~\177\5\22\n\2\177\u0080")
        buf.write(u"\7\26\2\2\u0080\u0081\b\t\1\2\u0081\u008b\3\2\2\2\u0082")
        buf.write(u"\u0083\7\n\2\2\u0083\u0084\7\25\2\2\u0084\u0085\5\4\3")
        buf.write(u"\2\u0085\u0086\7\30\2\2\u0086\u0087\5\6\4\2\u0087\u0088")
        buf.write(u"\7\26\2\2\u0088\u0089\b\t\1\2\u0089\u008b\3\2\2\2\u008a")
        buf.write(u"v\3\2\2\2\u008a|\3\2\2\2\u008a\u0082\3\2\2\2\u008b\21")
        buf.write(u"\3\2\2\2\u008c\u008d\5\4\3\2\u008d\u0094\b\n\1\2\u008e")
        buf.write(u"\u008f\7\30\2\2\u008f\u0090\5\4\3\2\u0090\u0091\b\n\1")
        buf.write(u"\2\u0091\u0093\3\2\2\2\u0092\u008e\3\2\2\2\u0093\u0096")
        buf.write(u"\3\2\2\2\u0094\u0092\3\2\2\2\u0094\u0095\3\2\2\2\u0095")
        buf.write(u"\23\3\2\2\2\u0096\u0094\3\2\2\2\f\34&*\65@Knt\u008a\u0094")
        return buf.getvalue()


class sigexpr_compiler ( Parser ):

    grammarFileName = "sigexpr_compiler.g"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ u"<INVALID>", u"<INVALID>", u"<INVALID>", u"'default'", 
                     u"'when'", u"'event'", u"'synchro'", u"'exclus'", u"'included'", 
                     u"'sequence'", u"'and'", u"'or'", u"'not'", u"'true'", 
                     u"'false'", u"'constraint'", u"':='", u"'='", u"'!='", 
                     u"'('", u"')'", u"'$'", u"','", u"'>'", u"'<'", u"'!'", 
                     u"';'", u"'+'", u"'-'", u"'*'", u"'^'" ]

    symbolicNames = [ u"<INVALID>", u"WS", u"COMMENT", u"DEFAULT", u"WHEN", 
                      u"EVENT", u"SYNC", u"EXC", u"INC", u"SEQ", u"AND", 
                      u"OR", u"NOT", u"T", u"F", u"CONSTR", u"DEF", u"EG", 
                      u"NOTEG", u"PG", u"PD", u"DOL", u"COM", u"UP", u"DOWN", 
                      u"CHG", u"SCOL", u"PLUS", u"MINUS", u"MUL", u"EXP", 
                      u"IDENT" ]

    RULE_sig_expression = 0
    RULE_sig_expression1 = 1
    RULE_sig_exp = 2
    RULE_sig_bool = 3
    RULE_sig_bool_and = 4
    RULE_sig_primary = 5
    RULE_sig_constant = 6
    RULE_sig_constraint = 7
    RULE_exp_list = 8

    ruleNames =  [ u"sig_expression", u"sig_expression1", u"sig_exp", u"sig_bool", 
                   u"sig_bool_and", u"sig_primary", u"sig_constant", u"sig_constraint", 
                   u"exp_list" ]

    EOF = Token.EOF
    WS=1
    COMMENT=2
    DEFAULT=3
    WHEN=4
    EVENT=5
    SYNC=6
    EXC=7
    INC=8
    SEQ=9
    AND=10
    OR=11
    NOT=12
    T=13
    F=14
    CONSTR=15
    DEF=16
    EG=17
    NOTEG=18
    PG=19
    PD=20
    DOL=21
    COM=22
    UP=23
    DOWN=24
    CHG=25
    SCOL=26
    PLUS=27
    MINUS=28
    MUL=29
    EXP=30
    IDENT=31

    def __init__(self, input, output=sys.stdout):
        super(sigexpr_compiler, self).__init__(input, output=output)
        self.checkVersion("4.8")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None



        # Extra attributes
        self.state_events = []
        self.free_clocks = []
        self.state_only = False
        self.state_only_in_bool = False
        self.catch_free_clocks = False
        self.deep = -1
        self.message = ""
        self.error_reporter = None

    def set_error_reporter(self, err):
        self.error_reporter = err

    def displayRecognitionError(self, tokens_names, re):
        """Display RecognitionError in the reporter"""
        # location of the error
        header = self.getErrorHeader(re)
        # message of the error
        error_msg = self.getErrorMessage(re, tokens_names)
        self.error_reporter.display(
            "RecognitionError: sig_exp -> {}; {}; {}".format(header, error_msg, self.message)
        )

    def displayExceptionMessage(self, e):
        """Display ExceptionMessage in the reporter"""
        error_msg = self.getErrorMessage(self, e, tokenNames)
        self.error_reporter.display("Exception: sig_exp -> " + error_msg)

    # semantic checks for compilers
    def check_ident(self, symbol_table, st_only, free_clock, deep, message, name):
        """Check if name is declared (with state/input type if st_only = True)

        @return: a SigIdentExpr
        """
        name = name.encode("utf-8")
        try:
            state_type, deepness = symbol_table[name]
            # for condition compiler
            if st_only and state_type not in ("state", "input"):
                self.error_reporter.display(
                    "Type error: '{}' has not an expected state: '{}'; {}".format(
                        name, state_type, message
                    )
                )
            elif deep >= 0 and deepness >= deep:
                self.error_reporter.display(
                    "Type error: '{}' not declared in a surrounding macro state; {}".format(
                        name, message
                    )
                )
        except KeyError:
            if free_clock and not st_only:
                self.free_clocks.append(name)
            else:
                self.error_reporter.display(
                    "Declaration error: Undeclared event or state: '{}'; {}".format(
                        name, message
                    )
                )
        return SigIdentExpr(name)

    def check_updown(self, symbol_table, id, mode):
        """
        This function introduce new signals: state> or state<
        """
        id = id.encode("utf-8")
        try:
            state_type, _ = symbol_table[id]
        except KeyError:
            self.error_reporter.display(
                "Declaration error: Undeclared state in variation: '{}'".format(id)
            )
            state_type = None

        if state_type == "state":
            # mode = 1: up: ">"
            # mode = 1: down: "<"
            name = id + (">" if mode == 1 else "<")

            self.state_events.append(name)
            return SigIdentExpr(name)
        else:
            self.error_reporter.display(
                "Type error: Up and Down can only be derived from a state: '{}'".format(id)
            )

    def check_change(self, symbol_table, id):
        id = id.encode("utf-8")
        try:
            state_type, _ = symbol_table[id]
        except KeyError:
            self.error_reporter.display(
                "Declaration error: Undeclared signal in variation: '{}'".format(id)
            )
            state_type = None

        if state_type == "state":
            refresh_expr = SigIdentExpr(id)
            st_expr = SigIdentExpr(id)
            return SigWhenExpr(SigConstExpr(True),SigDiffExpr(st_expr, refresh_expr))
        else:
            self.error_reporter.display(
                "Type error: Change can only be derived from a state: '{}'".format(id)
            )

    def check_sync(self, lexp):
        # type checking is done in expressions
        return SigConstraintExpr(SigConstraintExpr.SYNCHRO, lexp)

    def check_exclus(self, lexp):
        return SigConstraintExpr(SigConstraintExpr.EXCLU, lexp)

    def check_included(self, exp1,exp2):
        return SigConstraintExpr(SigConstraintExpr.INCL, [exp1,exp2])




    class Sig_expressionContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1, tab_symb=None):
            super(sigexpr_compiler.Sig_expressionContext, self).__init__(parent, invokingState)
            self.parser = parser
            self.tab_symb = None
            self.exp = None
            self.exp1 = None # Sig_expression1Context
            self.exp2 = None # Sig_constraintContext
            self.tab_symb = tab_symb

        def DOL(self):
            return self.getToken(sigexpr_compiler.DOL, 0)

        def sig_expression1(self):
            return self.getTypedRuleContext(sigexpr_compiler.Sig_expression1Context,0)


        def sig_constraint(self):
            return self.getTypedRuleContext(sigexpr_compiler.Sig_constraintContext,0)


        def getRuleIndex(self):
            return sigexpr_compiler.RULE_sig_expression




    def sig_expression(self, tab_symb):

        localctx = sigexpr_compiler.Sig_expressionContext(self, self._ctx, self.state, tab_symb)
        self.enterRule(localctx, 0, self.RULE_sig_expression)
        try:
            self.state = 26
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [sigexpr_compiler.WHEN, sigexpr_compiler.EVENT, sigexpr_compiler.NOT, sigexpr_compiler.T, sigexpr_compiler.F, sigexpr_compiler.PG, sigexpr_compiler.DOL, sigexpr_compiler.IDENT]:
                self.enterOuterAlt(localctx, 1)
                self.state = 18
                localctx.exp1 = self.sig_expression1(tab_symb)
                self.state = 19
                self.match(sigexpr_compiler.DOL)
                localctx.exp = localctx.exp1.exp
                pass
            elif token in [sigexpr_compiler.SYNC, sigexpr_compiler.EXC, sigexpr_compiler.INC]:
                self.enterOuterAlt(localctx, 2)
                self.state = 22
                localctx.exp2 = self.sig_constraint(tab_symb)
                self.state = 23
                self.match(sigexpr_compiler.DOL)
                localctx.exp = localctx.exp2.exp
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Sig_expression1Context(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1, tab_symb=None):
            super(sigexpr_compiler.Sig_expression1Context, self).__init__(parent, invokingState)
            self.parser = parser
            self.tab_symb = None
            self.exp = None
            self.exp1 = None # Sig_expContext
            self.exp2 = None # Sig_expContext
            self.tab_symb = tab_symb

        def sig_exp(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(sigexpr_compiler.Sig_expContext)
            else:
                return self.getTypedRuleContext(sigexpr_compiler.Sig_expContext,i)


        def DEFAULT(self, i=None):
            if i is None:
                return self.getTokens(sigexpr_compiler.DEFAULT)
            else:
                return self.getToken(sigexpr_compiler.DEFAULT, i)

        def getRuleIndex(self):
            return sigexpr_compiler.RULE_sig_expression1




    def sig_expression1(self, tab_symb):

        localctx = sigexpr_compiler.Sig_expression1Context(self, self._ctx, self.state, tab_symb)
        self.enterRule(localctx, 2, self.RULE_sig_expression1)
        self._la = 0 # Token type
        try:
            self.state = 40
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [sigexpr_compiler.WHEN, sigexpr_compiler.EVENT, sigexpr_compiler.NOT, sigexpr_compiler.T, sigexpr_compiler.F, sigexpr_compiler.PG, sigexpr_compiler.IDENT]:
                self.enterOuterAlt(localctx, 1)
                self.state = 28
                localctx.exp1 = self.sig_exp(tab_symb)
                localctx.exp = localctx.exp1.exp
                self.state = 36
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==sigexpr_compiler.DEFAULT:
                    self.state = 30
                    self.match(sigexpr_compiler.DEFAULT)
                    self.state = 31
                    localctx.exp2 = self.sig_exp(tab_symb)
                    localctx.exp = SigDefaultExpr(localctx.exp, localctx.exp2.exp)
                    self.state = 38
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                pass
            elif token in [sigexpr_compiler.PD, sigexpr_compiler.DOL, sigexpr_compiler.COM]:
                self.enterOuterAlt(localctx, 2)
                localctx.exp = SigConstExpr(True)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Sig_expContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1, tab_symb=None):
            super(sigexpr_compiler.Sig_expContext, self).__init__(parent, invokingState)
            self.parser = parser
            self.tab_symb = None
            self.exp = None
            self.exp1 = None # Sig_boolContext
            self.exp2 = None # Sig_boolContext
            self.tab_symb = tab_symb

        def sig_bool(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(sigexpr_compiler.Sig_boolContext)
            else:
                return self.getTypedRuleContext(sigexpr_compiler.Sig_boolContext,i)


        def WHEN(self, i=None):
            if i is None:
                return self.getTokens(sigexpr_compiler.WHEN)
            else:
                return self.getToken(sigexpr_compiler.WHEN, i)

        def getRuleIndex(self):
            return sigexpr_compiler.RULE_sig_exp




    def sig_exp(self, tab_symb):

        localctx = sigexpr_compiler.Sig_expContext(self, self._ctx, self.state, tab_symb)
        self.enterRule(localctx, 4, self.RULE_sig_exp)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 42
            localctx.exp1 = self.sig_bool(tab_symb)
            localctx.exp = localctx.exp1.exp
            self.state = 51
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==sigexpr_compiler.WHEN:
                self.state = 44
                self.match(sigexpr_compiler.WHEN)
                st_only_save = self.state_only; self.state_only = self.state_only_in_bool
                self.state = 46
                localctx.exp2 = self.sig_bool(tab_symb)

                localctx.exp = SigWhenExpr(localctx.exp, localctx.exp2.exp)
                self.state_only = st_only_save
                self.state = 53
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Sig_boolContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1, tab_symb=None):
            super(sigexpr_compiler.Sig_boolContext, self).__init__(parent, invokingState)
            self.parser = parser
            self.tab_symb = None
            self.exp = None
            self.exp1 = None # Sig_bool_andContext
            self.exp2 = None # Sig_bool_andContext
            self.tab_symb = tab_symb

        def sig_bool_and(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(sigexpr_compiler.Sig_bool_andContext)
            else:
                return self.getTypedRuleContext(sigexpr_compiler.Sig_bool_andContext,i)


        def OR(self, i=None):
            if i is None:
                return self.getTokens(sigexpr_compiler.OR)
            else:
                return self.getToken(sigexpr_compiler.OR, i)

        def getRuleIndex(self):
            return sigexpr_compiler.RULE_sig_bool




    def sig_bool(self, tab_symb):

        localctx = sigexpr_compiler.Sig_boolContext(self, self._ctx, self.state, tab_symb)
        self.enterRule(localctx, 6, self.RULE_sig_bool)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 54
            localctx.exp1 = self.sig_bool_and(tab_symb)
            localctx.exp = localctx.exp1.exp 
            self.state = 62
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==sigexpr_compiler.OR:
                self.state = 56
                self.match(sigexpr_compiler.OR)
                self.state = 57
                localctx.exp2 = self.sig_bool_and(tab_symb)
                localctx.exp = SigSyncBinExpr("or", localctx.exp, localctx.exp2.exp)
                self.state = 64
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Sig_bool_andContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1, tab_symb=None):
            super(sigexpr_compiler.Sig_bool_andContext, self).__init__(parent, invokingState)
            self.parser = parser
            self.tab_symb = None
            self.exp = None
            self.exp1 = None # Sig_primaryContext
            self.exp2 = None # Sig_primaryContext
            self.tab_symb = tab_symb

        def sig_primary(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(sigexpr_compiler.Sig_primaryContext)
            else:
                return self.getTypedRuleContext(sigexpr_compiler.Sig_primaryContext,i)


        def AND(self, i=None):
            if i is None:
                return self.getTokens(sigexpr_compiler.AND)
            else:
                return self.getToken(sigexpr_compiler.AND, i)

        def getRuleIndex(self):
            return sigexpr_compiler.RULE_sig_bool_and




    def sig_bool_and(self, tab_symb):

        localctx = sigexpr_compiler.Sig_bool_andContext(self, self._ctx, self.state, tab_symb)
        self.enterRule(localctx, 8, self.RULE_sig_bool_and)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 65
            localctx.exp1 = self.sig_primary(tab_symb)
            localctx.exp = localctx.exp1.exp 
            self.state = 73
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==sigexpr_compiler.AND:
                self.state = 67
                self.match(sigexpr_compiler.AND)
                self.state = 68
                localctx.exp2 = self.sig_primary(tab_symb)
                localctx.exp = SigSyncBinExpr("and",localctx.exp, localctx.exp2.exp)
                self.state = 75
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Sig_primaryContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1, tab_symb=None):
            super(sigexpr_compiler.Sig_primaryContext, self).__init__(parent, invokingState)
            self.parser = parser
            self.tab_symb = None
            self.exp = None
            self.nexp = None # Sig_primaryContext
            self.cexp = None # Sig_constantContext
            self.exps = None # Sig_expression1Context
            self.expw = None # Sig_expression1Context
            self.i7 = None # Token
            self.i8 = None # Token
            self.id2 = None # Token
            self.expse = None # Sig_expression1Context
            self.tab_symb = tab_symb

        def NOT(self):
            return self.getToken(sigexpr_compiler.NOT, 0)

        def sig_primary(self):
            return self.getTypedRuleContext(sigexpr_compiler.Sig_primaryContext,0)


        def sig_constant(self):
            return self.getTypedRuleContext(sigexpr_compiler.Sig_constantContext,0)


        def EVENT(self):
            return self.getToken(sigexpr_compiler.EVENT, 0)

        def PG(self):
            return self.getToken(sigexpr_compiler.PG, 0)

        def PD(self):
            return self.getToken(sigexpr_compiler.PD, 0)

        def sig_expression1(self):
            return self.getTypedRuleContext(sigexpr_compiler.Sig_expression1Context,0)


        def WHEN(self):
            return self.getToken(sigexpr_compiler.WHEN, 0)

        def UP(self):
            return self.getToken(sigexpr_compiler.UP, 0)

        def IDENT(self):
            return self.getToken(sigexpr_compiler.IDENT, 0)

        def DOWN(self):
            return self.getToken(sigexpr_compiler.DOWN, 0)

        def getRuleIndex(self):
            return sigexpr_compiler.RULE_sig_primary




    def sig_primary(self, tab_symb):

        localctx = sigexpr_compiler.Sig_primaryContext(self, self._ctx, self.state, tab_symb)
        self.enterRule(localctx, 10, self.RULE_sig_primary)
        try:
            self.state = 108
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,6,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 76
                self.match(sigexpr_compiler.NOT)
                self.state = 77
                localctx.nexp = self.sig_primary(tab_symb)
                localctx.exp = SigNotExpr(localctx.nexp.exp)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 80
                localctx.cexp = self.sig_constant()
                localctx.exp = localctx.cexp.exp
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 83
                self.match(sigexpr_compiler.EVENT)
                self.state = 84
                self.match(sigexpr_compiler.PG)
                self.state = 85
                localctx.exps = self.sig_expression1(tab_symb)
                self.state = 86
                self.match(sigexpr_compiler.PD)
                localctx.exp = SigEventExpr(localctx.exps.exp)
                pass

            elif la_ == 4:
                self.enterOuterAlt(localctx, 4)
                self.state = 89
                self.match(sigexpr_compiler.WHEN)
                self.state = 90
                self.match(sigexpr_compiler.PG)
                self.state = 91
                localctx.expw = self.sig_expression1(tab_symb)
                self.state = 92
                self.match(sigexpr_compiler.PD)
                localctx.exp = SigWhenExpr(SigConstExpr(True),localctx.expw.exp)
                pass

            elif la_ == 5:
                self.enterOuterAlt(localctx, 5)
                self.state = 95
                localctx.i7 = self.match(sigexpr_compiler.IDENT)
                self.state = 96
                self.match(sigexpr_compiler.UP)
                localctx.exp = self.check_updown(tab_symb, (None if localctx.i7 is None else localctx.i7.text), 1)
                pass

            elif la_ == 6:
                self.enterOuterAlt(localctx, 6)
                self.state = 98
                localctx.i8 = self.match(sigexpr_compiler.IDENT)
                self.state = 99
                self.match(sigexpr_compiler.DOWN)
                localctx.exp = self.check_updown(tab_symb, (None if localctx.i8 is None else localctx.i8.text), 2)
                pass

            elif la_ == 7:
                self.enterOuterAlt(localctx, 7)
                self.state = 101
                localctx.id2 = self.match(sigexpr_compiler.IDENT)
                localctx.exp = self.check_ident(tab_symb, self.state_only, self.catch_free_clocks, self.deep, self.message, (None if localctx.id2 is None else localctx.id2.text))
                pass

            elif la_ == 8:
                self.enterOuterAlt(localctx, 8)
                self.state = 103
                self.match(sigexpr_compiler.PG)
                self.state = 104
                localctx.expse = self.sig_expression1(tab_symb)
                self.state = 105
                self.match(sigexpr_compiler.PD)
                localctx.exp = localctx.expse.exp
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Sig_constantContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(sigexpr_compiler.Sig_constantContext, self).__init__(parent, invokingState)
            self.parser = parser
            self.exp = None

        def T(self):
            return self.getToken(sigexpr_compiler.T, 0)

        def F(self):
            return self.getToken(sigexpr_compiler.F, 0)

        def getRuleIndex(self):
            return sigexpr_compiler.RULE_sig_constant




    def sig_constant(self):

        localctx = sigexpr_compiler.Sig_constantContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_sig_constant)
        try:
            self.state = 114
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [sigexpr_compiler.T]:
                self.enterOuterAlt(localctx, 1)
                self.state = 110
                self.match(sigexpr_compiler.T)
                localctx.exp = SigConstExpr(True)
                pass
            elif token in [sigexpr_compiler.F]:
                self.enterOuterAlt(localctx, 2)
                self.state = 112
                self.match(sigexpr_compiler.F)
                localctx.exp = SigConstExpr(False)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Sig_constraintContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1, tab_symb=None):
            super(sigexpr_compiler.Sig_constraintContext, self).__init__(parent, invokingState)
            self.parser = parser
            self.tab_symb = None
            self.exp = None
            self.el = None # Exp_listContext
            self.e3 = None # Sig_expression1Context
            self.e4 = None # Sig_expContext
            self.tab_symb = tab_symb

        def SYNC(self):
            return self.getToken(sigexpr_compiler.SYNC, 0)

        def PG(self):
            return self.getToken(sigexpr_compiler.PG, 0)

        def PD(self):
            return self.getToken(sigexpr_compiler.PD, 0)

        def exp_list(self):
            return self.getTypedRuleContext(sigexpr_compiler.Exp_listContext,0)


        def EXC(self):
            return self.getToken(sigexpr_compiler.EXC, 0)

        def INC(self):
            return self.getToken(sigexpr_compiler.INC, 0)

        def COM(self):
            return self.getToken(sigexpr_compiler.COM, 0)

        def sig_expression1(self):
            return self.getTypedRuleContext(sigexpr_compiler.Sig_expression1Context,0)


        def sig_exp(self):
            return self.getTypedRuleContext(sigexpr_compiler.Sig_expContext,0)


        def getRuleIndex(self):
            return sigexpr_compiler.RULE_sig_constraint




    def sig_constraint(self, tab_symb):

        localctx = sigexpr_compiler.Sig_constraintContext(self, self._ctx, self.state, tab_symb)
        self.enterRule(localctx, 14, self.RULE_sig_constraint)
        try:
            self.state = 136
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [sigexpr_compiler.SYNC]:
                self.enterOuterAlt(localctx, 1)
                self.state = 116
                self.match(sigexpr_compiler.SYNC)
                self.state = 117
                self.match(sigexpr_compiler.PG)
                self.state = 118
                localctx.el = self.exp_list(tab_symb)
                self.state = 119
                self.match(sigexpr_compiler.PD)
                localctx.exp = self.check_sync(localctx.el.expl)
                pass
            elif token in [sigexpr_compiler.EXC]:
                self.enterOuterAlt(localctx, 2)
                self.state = 122
                self.match(sigexpr_compiler.EXC)
                self.state = 123
                self.match(sigexpr_compiler.PG)
                self.state = 124
                localctx.el = self.exp_list(tab_symb)
                self.state = 125
                self.match(sigexpr_compiler.PD)
                localctx.exp = self.check_exclus(localctx.el.expl)
                pass
            elif token in [sigexpr_compiler.INC]:
                self.enterOuterAlt(localctx, 3)
                self.state = 128
                self.match(sigexpr_compiler.INC)
                self.state = 129
                self.match(sigexpr_compiler.PG)
                self.state = 130
                localctx.e3 = self.sig_expression1(tab_symb)
                self.state = 131
                self.match(sigexpr_compiler.COM)
                self.state = 132
                localctx.e4 = self.sig_exp(tab_symb)
                self.state = 133
                self.match(sigexpr_compiler.PD)
                localctx.exp = self.check_included(localctx.e3.exp, localctx.e4.exp)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Exp_listContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1, tab_symb=None):
            super(sigexpr_compiler.Exp_listContext, self).__init__(parent, invokingState)
            self.parser = parser
            self.tab_symb = None
            self.expl = None
            self.exp1 = None # Sig_expression1Context
            self.exp2 = None # Sig_expression1Context
            self.tab_symb = tab_symb

        def sig_expression1(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(sigexpr_compiler.Sig_expression1Context)
            else:
                return self.getTypedRuleContext(sigexpr_compiler.Sig_expression1Context,i)


        def COM(self, i=None):
            if i is None:
                return self.getTokens(sigexpr_compiler.COM)
            else:
                return self.getToken(sigexpr_compiler.COM, i)

        def getRuleIndex(self):
            return sigexpr_compiler.RULE_exp_list




    def exp_list(self, tab_symb):

        localctx = sigexpr_compiler.Exp_listContext(self, self._ctx, self.state, tab_symb)
        self.enterRule(localctx, 16, self.RULE_exp_list)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 138
            localctx.exp1 = self.sig_expression1(tab_symb)
            localctx.expl = [localctx.exp1.exp]
            self.state = 146
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==sigexpr_compiler.COM:
                self.state = 140
                self.match(sigexpr_compiler.COM)
                self.state = 141
                localctx.exp2 = self.sig_expression1(tab_symb)
                localctx.expl.append(localctx.exp2.exp)
                self.state = 148
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





