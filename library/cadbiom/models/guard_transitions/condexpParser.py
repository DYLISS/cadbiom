# Generated from cadbiom/models/guard_transitions/condexp.g by ANTLR 4.8
# encoding: utf-8
from __future__ import print_function
from antlr4 import *
from io import StringIO
import sys




def serializedATN():
    with StringIO() as buf:
        buf.write(u"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3")
        buf.write(u"\r@\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\3\2\3\2\3")
        buf.write(u"\2\3\2\3\2\5\2\22\n\2\3\3\3\3\3\3\3\3\3\3\3\3\7\3\32")
        buf.write(u"\n\3\f\3\16\3\35\13\3\3\4\3\4\3\4\3\4\3\4\3\4\7\4%\n")
        buf.write(u"\4\f\4\16\4(\13\4\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5")
        buf.write(u"\3\5\3\5\3\5\3\5\3\5\5\58\n\5\3\6\3\6\3\6\3\6\5\6>\n")
        buf.write(u"\6\3\6\2\2\7\2\4\6\b\n\2\2\2A\2\21\3\2\2\2\4\23\3\2\2")
        buf.write(u"\2\6\36\3\2\2\2\b\67\3\2\2\2\n=\3\2\2\2\f\r\5\4\3\2\r")
        buf.write(u"\16\7\f\2\2\16\17\b\2\1\2\17\22\3\2\2\2\20\22\7\f\2\2")
        buf.write(u"\21\f\3\2\2\2\21\20\3\2\2\2\22\3\3\2\2\2\23\24\5\6\4")
        buf.write(u"\2\24\33\b\3\1\2\25\26\7\6\2\2\26\27\5\6\4\2\27\30\b")
        buf.write(u"\3\1\2\30\32\3\2\2\2\31\25\3\2\2\2\32\35\3\2\2\2\33\31")
        buf.write(u"\3\2\2\2\33\34\3\2\2\2\34\5\3\2\2\2\35\33\3\2\2\2\36")
        buf.write(u"\37\5\b\5\2\37&\b\4\1\2 !\7\5\2\2!\"\5\b\5\2\"#\b\4\1")
        buf.write(u"\2#%\3\2\2\2$ \3\2\2\2%(\3\2\2\2&$\3\2\2\2&\'\3\2\2\2")
        buf.write(u"\'\7\3\2\2\2(&\3\2\2\2)*\7\7\2\2*+\5\b\5\2+,\b\5\1\2")
        buf.write(u",8\3\2\2\2-.\5\n\6\2./\b\5\1\2/8\3\2\2\2\60\61\7\r\2")
        buf.write(u"\2\618\b\5\1\2\62\63\7\n\2\2\63\64\5\4\3\2\64\65\7\13")
        buf.write(u"\2\2\65\66\b\5\1\2\668\3\2\2\2\67)\3\2\2\2\67-\3\2\2")
        buf.write(u"\2\67\60\3\2\2\2\67\62\3\2\2\28\t\3\2\2\29:\7\b\2\2:")
        buf.write(u">\b\6\1\2;<\7\t\2\2<>\b\6\1\2=9\3\2\2\2=;\3\2\2\2>\13")
        buf.write(u"\3\2\2\2\7\21\33&\67=")
        return buf.getvalue()


class condexpParser ( Parser ):

    grammarFileName = "condexp.g"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ u"<INVALID>", u"<INVALID>", u"<INVALID>", u"'and'", 
                     u"'or'", u"'not'", u"'true'", u"'false'", u"'('", u"')'", 
                     u"'$'" ]

    symbolicNames = [ u"<INVALID>", u"WS", u"COMMENT", u"AND", u"OR", u"NOT", 
                      u"T", u"F", u"PG", u"PD", u"DOL", u"IDENT" ]

    RULE_sig_bool = 0
    RULE_sig_bool1 = 1
    RULE_sig_bool_and = 2
    RULE_sig_primary = 3
    RULE_sig_constant = 4

    ruleNames =  [ u"sig_bool", u"sig_bool1", u"sig_bool_and", u"sig_primary", 
                   u"sig_constant" ]

    EOF = Token.EOF
    WS=1
    COMMENT=2
    AND=3
    OR=4
    NOT=5
    T=6
    F=7
    PG=8
    PD=9
    DOL=10
    IDENT=11

    def __init__(self, input, output=sys.stdout):
        super(condexpParser, self).__init__(input, output=output)
        self.checkVersion("4.8")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class Sig_boolContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(condexpParser.Sig_boolContext, self).__init__(parent, invokingState)
            self.parser = parser
            self.id1 = None # Sig_bool1Context

        def DOL(self):
            return self.getToken(condexpParser.DOL, 0)

        def sig_bool1(self):
            return self.getTypedRuleContext(condexpParser.Sig_bool1Context,0)


        def getRuleIndex(self):
            return condexpParser.RULE_sig_bool




    def sig_bool(self):

        localctx = condexpParser.Sig_boolContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_sig_bool)
        localctx.idents = set()
        try:
            self.state = 15
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [condexpParser.NOT, condexpParser.T, condexpParser.F, condexpParser.PG, condexpParser.IDENT]:
                self.enterOuterAlt(localctx, 1)
                self.state = 10
                localctx.id1 = self.sig_bool1()
                self.state = 11
                self.match(condexpParser.DOL)
                localctx.idents = localctx.id1.idents
                pass
            elif token in [condexpParser.DOL]:
                self.enterOuterAlt(localctx, 2)
                self.state = 14
                self.match(condexpParser.DOL)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Sig_bool1Context(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(condexpParser.Sig_bool1Context, self).__init__(parent, invokingState)
            self.parser = parser
            self.id1 = None # Sig_bool_andContext
            self.id2 = None # Sig_bool_andContext

        def sig_bool_and(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(condexpParser.Sig_bool_andContext)
            else:
                return self.getTypedRuleContext(condexpParser.Sig_bool_andContext,i)


        def OR(self, i=None):
            if i is None:
                return self.getTokens(condexpParser.OR)
            else:
                return self.getToken(condexpParser.OR, i)

        def getRuleIndex(self):
            return condexpParser.RULE_sig_bool1




    def sig_bool1(self):

        localctx = condexpParser.Sig_bool1Context(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_sig_bool1)
        localctx.idents = set()
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 17
            localctx.id1 = self.sig_bool_and()
            localctx.idents = localctx.id1.idents
            self.state = 25
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==condexpParser.OR:
                self.state = 19
                self.match(condexpParser.OR)
                self.state = 20
                localctx.id2 = self.sig_bool_and()
                localctx.idents |= localctx.id2.idents
                self.state = 27
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Sig_bool_andContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(condexpParser.Sig_bool_andContext, self).__init__(parent, invokingState)
            self.parser = parser
            self.id1 = None # Sig_primaryContext
            self.id2 = None # Sig_primaryContext

        def sig_primary(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(condexpParser.Sig_primaryContext)
            else:
                return self.getTypedRuleContext(condexpParser.Sig_primaryContext,i)


        def AND(self, i=None):
            if i is None:
                return self.getTokens(condexpParser.AND)
            else:
                return self.getToken(condexpParser.AND, i)

        def getRuleIndex(self):
            return condexpParser.RULE_sig_bool_and




    def sig_bool_and(self):

        localctx = condexpParser.Sig_bool_andContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_sig_bool_and)
        localctx.idents = set()
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 28
            localctx.id1 = self.sig_primary()
            localctx.idents = localctx.id1.idents
            self.state = 36
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==condexpParser.AND:
                self.state = 30
                self.match(condexpParser.AND)
                self.state = 31
                localctx.id2 = self.sig_primary()
                localctx.idents |= localctx.id2.idents
                self.state = 38
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Sig_primaryContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(condexpParser.Sig_primaryContext, self).__init__(parent, invokingState)
            self.parser = parser
            self.id1 = None # Sig_primaryContext
            self.id4 = None # Sig_constantContext
            self.id2 = None # Token
            self.id3 = None # Sig_bool1Context

        def NOT(self):
            return self.getToken(condexpParser.NOT, 0)

        def sig_primary(self):
            return self.getTypedRuleContext(condexpParser.Sig_primaryContext,0)


        def sig_constant(self):
            return self.getTypedRuleContext(condexpParser.Sig_constantContext,0)


        def IDENT(self):
            return self.getToken(condexpParser.IDENT, 0)

        def PG(self):
            return self.getToken(condexpParser.PG, 0)

        def PD(self):
            return self.getToken(condexpParser.PD, 0)

        def sig_bool1(self):
            return self.getTypedRuleContext(condexpParser.Sig_bool1Context,0)


        def getRuleIndex(self):
            return condexpParser.RULE_sig_primary




    def sig_primary(self):

        localctx = condexpParser.Sig_primaryContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_sig_primary)
        localctx.idents = set()
        try:
            self.state = 53
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [condexpParser.NOT]:
                self.enterOuterAlt(localctx, 1)
                self.state = 39
                self.match(condexpParser.NOT)
                self.state = 40
                localctx.id1 = self.sig_primary()
                localctx.idents = localctx.id1.idents
                pass
            elif token in [condexpParser.T, condexpParser.F]:
                self.enterOuterAlt(localctx, 2)
                self.state = 43
                localctx.id4 = self.sig_constant()
                localctx.idents = localctx.id4.idents
                pass
            elif token in [condexpParser.IDENT]:
                self.enterOuterAlt(localctx, 3)
                self.state = 46
                localctx.id2 = self.match(condexpParser.IDENT)
                localctx.idents = set([(None if localctx.id2 is None else localctx.id2.text).encode("utf8")])
                pass
            elif token in [condexpParser.PG]:
                self.enterOuterAlt(localctx, 4)
                self.state = 48
                self.match(condexpParser.PG)
                self.state = 49
                localctx.id3 = self.sig_bool1()
                self.state = 50
                self.match(condexpParser.PD)
                localctx.idents = localctx.id3.idents
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Sig_constantContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(condexpParser.Sig_constantContext, self).__init__(parent, invokingState)
            self.parser = parser

        def T(self):
            return self.getToken(condexpParser.T, 0)

        def F(self):
            return self.getToken(condexpParser.F, 0)

        def getRuleIndex(self):
            return condexpParser.RULE_sig_constant




    def sig_constant(self):

        localctx = condexpParser.Sig_constantContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_sig_constant)
        localctx.idents = set()
        try:
            self.state = 59
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [condexpParser.T]:
                self.enterOuterAlt(localctx, 1)
                self.state = 55
                self.match(condexpParser.T)
                localctx.idents = set()
                pass
            elif token in [condexpParser.F]:
                self.enterOuterAlt(localctx, 2)
                self.state = 57
                self.match(condexpParser.F)
                localctx.idents = set()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





