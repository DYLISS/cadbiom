# Generated from cadbiom/models/guard_transitions/translators/pintlang.g by ANTLR 4.8
# encoding: utf-8
from __future__ import print_function
from antlr4 import *
from io import StringIO
import sys



def serializedATN():
    with StringIO() as buf:
        buf.write(u"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2")
        buf.write(u"\23~\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7")
        buf.write(u"\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t")
        buf.write(u"\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22")
        buf.write(u"\4\23\t\23\4\24\t\24\3\2\3\2\3\2\3\2\3\3\3\3\3\3\3\3")
        buf.write(u"\7\3\62\n\3\f\3\16\3\65\13\3\3\3\3\3\3\3\3\3\3\4\3\4")
        buf.write(u"\3\4\3\4\3\4\3\4\3\4\3\4\3\5\3\5\3\5\3\6\3\6\3\6\3\6")
        buf.write(u"\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\7\3\7\3\7")
        buf.write(u"\3\b\3\b\3\t\3\t\3\n\3\n\3\13\3\13\3\f\3\f\3\r\3\r\3")
        buf.write(u"\16\3\16\3\16\3\16\3\17\3\17\3\17\3\20\3\20\3\20\3\20")
        buf.write(u"\3\21\3\21\3\22\3\22\3\23\3\23\3\23\7\23u\n\23\f\23\16")
        buf.write(u"\23x\13\23\3\24\6\24{\n\24\r\24\16\24|\2\2\25\3\3\5\4")
        buf.write(u"\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16\33")
        buf.write(u"\17\35\20\37\21!\2#\2%\22\'\23\3\2\5\4\2\13\f\"\"\3\2")
        buf.write(u"\f\f\5\2C\\aac|\2\177\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2")
        buf.write(u"\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2")
        buf.write(u"\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2")
        buf.write(u"\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2")
        buf.write(u"\2\2%\3\2\2\2\2\'\3\2\2\2\3)\3\2\2\2\5-\3\2\2\2\7:\3")
        buf.write(u"\2\2\2\tB\3\2\2\2\13E\3\2\2\2\rS\3\2\2\2\17V\3\2\2\2")
        buf.write(u"\21X\3\2\2\2\23Z\3\2\2\2\25\\\3\2\2\2\27^\3\2\2\2\31")
        buf.write(u"`\3\2\2\2\33b\3\2\2\2\35f\3\2\2\2\37i\3\2\2\2!m\3\2\2")
        buf.write(u"\2#o\3\2\2\2%q\3\2\2\2\'z\3\2\2\2)*\t\2\2\2*+\3\2\2\2")
        buf.write(u"+,\b\2\2\2,\4\3\2\2\2-.\7\61\2\2./\7\61\2\2/\63\3\2\2")
        buf.write(u"\2\60\62\n\3\2\2\61\60\3\2\2\2\62\65\3\2\2\2\63\61\3")
        buf.write(u"\2\2\2\63\64\3\2\2\2\64\66\3\2\2\2\65\63\3\2\2\2\66\67")
        buf.write(u"\7\f\2\2\678\3\2\2\289\b\3\2\29\6\3\2\2\2:;\7r\2\2;<")
        buf.write(u"\7t\2\2<=\7q\2\2=>\7e\2\2>?\7g\2\2?@\7u\2\2@A\7u\2\2")
        buf.write(u"A\b\3\2\2\2BC\7/\2\2CD\7@\2\2D\n\3\2\2\2EF\7E\2\2FG\7")
        buf.write(u"Q\2\2GH\7Q\2\2HI\7R\2\2IJ\7G\2\2JK\7T\2\2KL\7C\2\2LM")
        buf.write(u"\7V\2\2MN\7K\2\2NO\7X\2\2OP\7K\2\2PQ\7V\2\2QR\7[\2\2")
        buf.write(u"R\f\3\2\2\2ST\7k\2\2TU\7p\2\2U\16\3\2\2\2VW\7*\2\2W\20")
        buf.write(u"\3\2\2\2XY\7+\2\2Y\22\3\2\2\2Z[\7]\2\2[\24\3\2\2\2\\")
        buf.write(u"]\7_\2\2]\26\3\2\2\2^_\7=\2\2_\30\3\2\2\2`a\7.\2\2a\32")
        buf.write(u"\3\2\2\2bc\7c\2\2cd\7p\2\2de\7f\2\2e\34\3\2\2\2fg\7q")
        buf.write(u"\2\2gh\7t\2\2h\36\3\2\2\2ij\7p\2\2jk\7q\2\2kl\7v\2\2")
        buf.write(u"l \3\2\2\2mn\t\4\2\2n\"\3\2\2\2op\4\62;\2p$\3\2\2\2q")
        buf.write(u"v\5!\21\2ru\5!\21\2su\5#\22\2tr\3\2\2\2ts\3\2\2\2ux\3")
        buf.write(u"\2\2\2vt\3\2\2\2vw\3\2\2\2w&\3\2\2\2xv\3\2\2\2y{\5#\22")
        buf.write(u"\2zy\3\2\2\2{|\3\2\2\2|z\3\2\2\2|}\3\2\2\2}(\3\2\2\2")
        buf.write(u"\7\2\63tv|\3\2\3\2")
        return buf.getvalue()


class pintlangLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    WS = 1
    COMMENT = 2
    PROC = 3
    ARROW = 4
    COOP = 5
    IN_KW = 6
    LP = 7
    RP = 8
    LB = 9
    RB = 10
    SC = 11
    COM = 12
    AND = 13
    OR = 14
    NOT = 15
    IDENT = 16
    INT = 17

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ u"DEFAULT_MODE" ]

    literalNames = [ u"<INVALID>",
            u"'process'", u"'->'", u"'COOPERATIVITY'", u"'in'", u"'('", 
            u"')'", u"'['", u"']'", u"';'", u"','", u"'and'", u"'or'", u"'not'" ]

    symbolicNames = [ u"<INVALID>",
            u"WS", u"COMMENT", u"PROC", u"ARROW", u"COOP", u"IN_KW", u"LP", 
            u"RP", u"LB", u"RB", u"SC", u"COM", u"AND", u"OR", u"NOT", u"IDENT", 
            u"INT" ]

    ruleNames = [ u"WS", u"COMMENT", u"PROC", u"ARROW", u"COOP", u"IN_KW", 
                  u"LP", u"RP", u"LB", u"RB", u"SC", u"COM", u"AND", u"OR", 
                  u"NOT", u"LETTER", u"DIGIT", u"IDENT", u"INT" ]

    grammarFileName = u"pintlang.g"

    def __init__(self, input=None, output=sys.stdout):
        super(pintlangLexer, self).__init__(input, output=output)
        self.checkVersion("4.8")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


    def set_error_reporter(self, err):
        self.error_reporter = err

    def displayRecognitionError(self, tokenNames, re):
        hdr = self.getErrorHeader(re)
        msg = self.getErrorMessage(re, tokenNames)
        self.error_reporter.display(hdr,msg)
        

    def displayExceptionMessage(self, e):
        msg = self.getErrorMessage(self, e, tokenNames)
        self.error_reporter.display('',msg)


