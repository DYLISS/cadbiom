# Generated from cadbiom/models/guard_transitions/translators/pintlang.g by ANTLR 4.8
# encoding: utf-8
from __future__ import print_function
from antlr4 import *
from io import StringIO
import sys


from cadbiom.models.guard_transitions.chart_model import ChartModel


def serializedATN():
    with StringIO() as buf:
        buf.write(u"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3")
        buf.write(u"\23\u00b5\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7")
        buf.write(u"\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t")
        buf.write(u"\r\4\16\t\16\4\17\t\17\3\2\3\2\3\2\7\2\"\n\2\f\2\16\2")
        buf.write(u"%\13\2\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3\4\3\4\3\4\3\4\3")
        buf.write(u"\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\6\3\6\3")
        buf.write(u"\6\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\5")
        buf.write(u"\7L\n\7\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\5\bW\n\b")
        buf.write(u"\3\t\3\t\3\t\3\t\3\t\3\t\7\t_\n\t\f\t\16\tb\13\t\3\t")
        buf.write(u"\3\t\3\n\3\n\3\n\3\n\3\n\3\n\3\n\7\nm\n\n\f\n\16\np\13")
        buf.write(u"\n\3\n\3\n\3\13\3\13\3\13\3\13\3\13\3\13\7\13z\n\13\f")
        buf.write(u"\13\16\13}\13\13\3\13\3\13\3\f\3\f\3\f\3\f\3\f\3\f\3")
        buf.write(u"\f\3\f\3\f\3\f\3\f\3\f\5\f\u008d\n\f\3\r\3\r\3\r\3\r")
        buf.write(u"\3\r\3\r\7\r\u0095\n\r\f\r\16\r\u0098\13\r\3\16\3\16")
        buf.write(u"\3\16\3\16\3\16\3\16\7\16\u00a0\n\16\f\16\16\16\u00a3")
        buf.write(u"\13\16\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3")
        buf.write(u"\17\3\17\3\17\3\17\3\17\5\17\u00b3\n\17\3\17\2\2\20\2")
        buf.write(u"\4\6\b\n\f\16\20\22\24\26\30\32\34\2\2\2\u00b5\2#\3\2")
        buf.write(u"\2\2\4(\3\2\2\2\6-\3\2\2\2\b\65\3\2\2\2\n<\3\2\2\2\f")
        buf.write(u"K\3\2\2\2\16V\3\2\2\2\20X\3\2\2\2\22e\3\2\2\2\24s\3\2")
        buf.write(u"\2\2\26\u008c\3\2\2\2\30\u008e\3\2\2\2\32\u0099\3\2\2")
        buf.write(u"\2\34\u00b2\3\2\2\2\36\"\5\4\3\2\37\"\5\6\4\2 \"\5\b")
        buf.write(u"\5\2!\36\3\2\2\2!\37\3\2\2\2! \3\2\2\2\"%\3\2\2\2#!\3")
        buf.write(u"\2\2\2#$\3\2\2\2$&\3\2\2\2%#\3\2\2\2&\'\7\2\2\3\'\3\3")
        buf.write(u"\2\2\2()\7\5\2\2)*\7\22\2\2*+\7\23\2\2+,\b\3\1\2,\5\3")
        buf.write(u"\2\2\2-.\7\22\2\2./\7\23\2\2/\60\7\6\2\2\60\61\7\22\2")
        buf.write(u"\2\61\62\7\23\2\2\62\63\7\23\2\2\63\64\b\4\1\2\64\7\3")
        buf.write(u"\2\2\2\65\66\7\7\2\2\66\67\7\t\2\2\678\5\n\6\289\7\16")
        buf.write(u"\2\29:\5\16\b\2:;\7\n\2\2;\t\3\2\2\2<=\5\20\t\2=>\b\6")
        buf.write(u"\1\2>?\5\f\7\2?@\b\6\1\2@\13\3\2\2\2AB\7\b\2\2BC\5\22")
        buf.write(u"\n\2CD\5\26\f\2DE\b\7\1\2EL\3\2\2\2FG\7\6\2\2GH\7\22")
        buf.write(u"\2\2HI\7\23\2\2IJ\7\23\2\2JL\b\7\1\2KA\3\2\2\2KF\3\2")
        buf.write(u"\2\2L\r\3\2\2\2MN\5\22\n\2NO\b\b\1\2OW\3\2\2\2PQ\7\22")
        buf.write(u"\2\2QR\7\16\2\2RS\7\23\2\2ST\7\16\2\2TU\7\23\2\2UW\b")
        buf.write(u"\b\1\2VM\3\2\2\2VP\3\2\2\2W\17\3\2\2\2XY\7\13\2\2YZ\7")
        buf.write(u"\22\2\2Z`\b\t\1\2[\\\7\r\2\2\\]\7\22\2\2]_\b\t\1\2^[")
        buf.write(u"\3\2\2\2_b\3\2\2\2`^\3\2\2\2`a\3\2\2\2ac\3\2\2\2b`\3")
        buf.write(u"\2\2\2cd\7\f\2\2d\21\3\2\2\2ef\7\13\2\2fg\5\24\13\2g")
        buf.write(u"n\b\n\1\2hi\7\r\2\2ij\5\24\13\2jk\b\n\1\2km\3\2\2\2l")
        buf.write(u"h\3\2\2\2mp\3\2\2\2nl\3\2\2\2no\3\2\2\2oq\3\2\2\2pn\3")
        buf.write(u"\2\2\2qr\7\f\2\2r\23\3\2\2\2st\7\13\2\2tu\7\23\2\2u{")
        buf.write(u"\b\13\1\2vw\7\r\2\2wx\7\23\2\2xz\b\13\1\2yv\3\2\2\2z")
        buf.write(u"}\3\2\2\2{y\3\2\2\2{|\3\2\2\2|~\3\2\2\2}{\3\2\2\2~\177")
        buf.write(u"\7\f\2\2\177\25\3\2\2\2\u0080\u008d\b\f\1\2\u0081\u0082")
        buf.write(u"\7\17\2\2\u0082\u0083\5\30\r\2\u0083\u0084\b\f\1\2\u0084")
        buf.write(u"\u008d\3\2\2\2\u0085\u0086\7\20\2\2\u0086\u0087\5\30")
        buf.write(u"\r\2\u0087\u0088\b\f\1\2\u0088\u008d\3\2\2\2\u0089\u008a")
        buf.write(u"\5\34\17\2\u008a\u008b\b\f\1\2\u008b\u008d\3\2\2\2\u008c")
        buf.write(u"\u0080\3\2\2\2\u008c\u0081\3\2\2\2\u008c\u0085\3\2\2")
        buf.write(u"\2\u008c\u0089\3\2\2\2\u008d\27\3\2\2\2\u008e\u008f\5")
        buf.write(u"\32\16\2\u008f\u0096\b\r\1\2\u0090\u0091\7\20\2\2\u0091")
        buf.write(u"\u0092\5\32\16\2\u0092\u0093\b\r\1\2\u0093\u0095\3\2")
        buf.write(u"\2\2\u0094\u0090\3\2\2\2\u0095\u0098\3\2\2\2\u0096\u0094")
        buf.write(u"\3\2\2\2\u0096\u0097\3\2\2\2\u0097\31\3\2\2\2\u0098\u0096")
        buf.write(u"\3\2\2\2\u0099\u009a\5\34\17\2\u009a\u00a1\b\16\1\2\u009b")
        buf.write(u"\u009c\7\17\2\2\u009c\u009d\5\34\17\2\u009d\u009e\b\16")
        buf.write(u"\1\2\u009e\u00a0\3\2\2\2\u009f\u009b\3\2\2\2\u00a0\u00a3")
        buf.write(u"\3\2\2\2\u00a1\u009f\3\2\2\2\u00a1\u00a2\3\2\2\2\u00a2")
        buf.write(u"\33\3\2\2\2\u00a3\u00a1\3\2\2\2\u00a4\u00a5\7\21\2\2")
        buf.write(u"\u00a5\u00a6\5\34\17\2\u00a6\u00a7\b\17\1\2\u00a7\u00b3")
        buf.write(u"\3\2\2\2\u00a8\u00a9\5\20\t\2\u00a9\u00aa\7\b\2\2\u00aa")
        buf.write(u"\u00ab\5\22\n\2\u00ab\u00ac\b\17\1\2\u00ac\u00b3\3\2")
        buf.write(u"\2\2\u00ad\u00ae\7\t\2\2\u00ae\u00af\5\30\r\2\u00af\u00b0")
        buf.write(u"\7\n\2\2\u00b0\u00b1\b\17\1\2\u00b1\u00b3\3\2\2\2\u00b2")
        buf.write(u"\u00a4\3\2\2\2\u00b2\u00a8\3\2\2\2\u00b2\u00ad\3\2\2")
        buf.write(u"\2\u00b3\35\3\2\2\2\r!#KV`n{\u008c\u0096\u00a1\u00b2")
        return buf.getvalue()


class pintlangParser ( Parser ):

    grammarFileName = "pintlang.g"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ u"<INVALID>", u"<INVALID>", u"<INVALID>", u"'process'", 
                     u"'->'", u"'COOPERATIVITY'", u"'in'", u"'('", u"')'", 
                     u"'['", u"']'", u"';'", u"','", u"'and'", u"'or'", 
                     u"'not'" ]

    symbolicNames = [ u"<INVALID>", u"WS", u"COMMENT", u"PROC", u"ARROW", 
                      u"COOP", u"IN_KW", u"LP", u"RP", u"LB", u"RB", u"SC", 
                      u"COM", u"AND", u"OR", u"NOT", u"IDENT", u"INT" ]

    RULE_pintspec = 0
    RULE_process = 1
    RULE_action = 2
    RULE_cooper = 3
    RULE_coop_lhs = 4
    RULE_rcoop_lhs = 5
    RULE_coop_rhs = 6
    RULE_sort_list = 7
    RULE_tt_condition = 8
    RULE_tt_conjunction = 9
    RULE_tail_logexp = 10
    RULE_tt_logexp = 11
    RULE_tt_logexp2 = 12
    RULE_tt_primary = 13

    ruleNames =  [ u"pintspec", u"process", u"action", u"cooper", u"coop_lhs", 
                   u"rcoop_lhs", u"coop_rhs", u"sort_list", u"tt_condition", 
                   u"tt_conjunction", u"tail_logexp", u"tt_logexp", u"tt_logexp2", 
                   u"tt_primary" ]

    EOF = Token.EOF
    WS=1
    COMMENT=2
    PROC=3
    ARROW=4
    COOP=5
    IN_KW=6
    LP=7
    RP=8
    LB=9
    RB=10
    SC=11
    COM=12
    AND=13
    OR=14
    NOT=15
    IDENT=16
    INT=17

    def __init__(self, input, output=sys.stdout):
        super(pintlangParser, self).__init__(input, output=output)
        self.checkVersion("4.8")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None



    def set_error_reporter(self, err):
        self.error_reporter = err

    def displayRecognitionError(self, tokenNames, re):
        hdr = self.getErrorHeader(re)
        msg = self.getErrorMessage(re, tokenNames)
        self.error_reporter.display(hdr,msg)
        

    def displayExceptionMessage(self, e):
        msg = self.getErrorMessage(self, e, tokenNames)
        self.error_reporter.display('',msg)
        
    def check_process(self, name, level, line):
        line_txt = 'line '+str(line)+':'
        try:
            lev = self.symb_tab[name]
            self.error_reporter.display(line_txt, "Process double declaration: "+id)
        
        except KeyError:
            self.symb_tab[name] = level
            for ii in range(level+1):
                pname = name + '_' + str(ii)
                self.model.get_root().add_simple_node(pname, 0.0, 0.0)
                
    def check_action(self, fname, flevel, cname, clevel1, clevel2, line):
        line_txt = 'line '+str(line)+':'
        try:
            lev = self.symb_tab[fname]
            if flevel>lev:
                self.error_reporter(line_txt, "Incorrect level for:"+fname)
                return
        except KeyError:
            self.error_reporter.display(line_txt, "Undeclared process: "+fname)
            return
        try:
            lev = self.symb_tab[cname]
            if clevel1>lev or clevel2>lev:
                self.error_reporter(line_txt, "Incorrect level for:"+cname)
                return
        except KeyError:
            self.error_reporter.display(line_txt, "Undeclared process: "+cname)
            return
            
        n1 = self.model.get_simple_node(cname+'_'+str(clevel1))
        n2 = self.model.get_simple_node(cname+'_'+str(clevel2))
        tr=self.model.get_root().add_transition(n1, n2)
        tr.set_condition(fname+'_'+str(flevel))
        h_name = 'hh_'+str(self.clock_cpt)
        self.clock_cpt +=1
        tr.set_event(h_name)
        self.clock_list.append(h_name)
        return
        
    def check_transition(self, cname, clevel1, clevel2, line):
        line_txt = 'line '+str(line)+':'
        try:
            lev = self.symb_tab[cname]
            if clevel1>lev or clevel2>lev:
                self.error_reporter(line_txt, "Incorrect level for:"+cname)
                return
        except KeyError:
            self.error_reporter.display(line_txt, "Undeclared process: "+cname)
            return
            
        n1 = self.model.get_simple_node(cname+'_'+str(clevel1))
        n2 = self.model.get_simple_node(cname+'_'+str(clevel2))
        tr=self.model.get_root().add_transition(n1, n2)
        return tr
        
    def check_tt_rhs(self, tr, il ,l_cond):
        line_txt = 'line '+'? :'
        cond = self.tt_translate_lcond(il, l_cond, line_txt)
        tr.set_condition(cond)
        h_name = 'hh_'+str(self.clock_cpt)
        self.clock_cpt +=1
        tr.set_event(h_name)
        self.clock_list.append(h_name)
        
    def check_tr_rhs(self, cond, cname, clevel1, clevel2, line):
        line_txt = 'line '+str(line)+':'
        try:
            lev = self.symb_tab[cname]
            if clevel1>lev or clevel2>lev:
                self.error_reporter(line_txt, "Incorrect level for: "+cname)
                return
        except KeyError:
            self.error_reporter.display(line_txt, "Undeclared process: "+cname)
            return
            
        n1 = self.model.get_simple_node(cname+'_'+str(clevel1))
        n2 = self.model.get_simple_node(cname+'_'+str(clevel2))
        tr=self.model.get_root().add_transition(n1, n2)
        tr.set_condition(cond)
        h_name = 'hh_'+str(self.clock_cpt)
        self.clock_cpt +=1
        tr.set_event(h_name)
        self.clock_list.append(h_name)
        
        
    def tt_translate_cond(self, id_list, val_list, line_txt):
        if len(id_list) != len(val_list):
            self.error_reporter.display(line_txt, "Bad condition specification " + str(id_list))
            return None
        cond = id_list[0] + "_"+str(val_list[0])
        for i in range(1, len(id_list)):
            cond_el = id_list[i] + "_" + str(val_list[i])
            cond = cond + " and "+ cond_el
        return cond
        
        
    def tt_translate_lcond(self, id_list, lval_list, line_txt):
        cond_el = self.tt_translate_cond(id_list, lval_list[0], line_txt)
        if cond_el:
            cond = '(' + cond_el + ')'
        else:
            return ""
        for i in range(1,len(lval_list)):
            cond_el = self.tt_translate_cond(id_list, lval_list[i], line_txt)
            if cond_el:
                cond = cond + ' or (' + cond_el + ')'
            else:
                return ""
        return cond
        




    class PintspecContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1, model_name=None):
            super(pintlangParser.PintspecContext, self).__init__(parent, invokingState)
            self.parser = parser
            self.model_name = None
            self.model_name = model_name

        def EOF(self):
            return self.getToken(pintlangParser.EOF, 0)

        def process(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(pintlangParser.ProcessContext)
            else:
                return self.getTypedRuleContext(pintlangParser.ProcessContext,i)


        def action(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(pintlangParser.ActionContext)
            else:
                return self.getTypedRuleContext(pintlangParser.ActionContext,i)


        def cooper(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(pintlangParser.CooperContext)
            else:
                return self.getTypedRuleContext(pintlangParser.CooperContext,i)


        def getRuleIndex(self):
            return pintlangParser.RULE_pintspec




    def pintspec(self, model_name):

        localctx = pintlangParser.PintspecContext(self, self._ctx, self.state, model_name)
        self.enterRule(localctx, 0, self.RULE_pintspec)

        self.model = ChartModel(model_name)
        self.symb_tab = dict()
        self.clock_cpt = 0
        self.clock_list = []
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 33
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << pintlangParser.PROC) | (1 << pintlangParser.COOP) | (1 << pintlangParser.IDENT))) != 0):
                self.state = 31
                self._errHandler.sync(self)
                token = self._input.LA(1)
                if token in [pintlangParser.PROC]:
                    self.state = 28
                    self.process()
                    pass
                elif token in [pintlangParser.IDENT]:
                    self.state = 29
                    self.action()
                    pass
                elif token in [pintlangParser.COOP]:
                    self.state = 30
                    self.cooper()
                    pass
                else:
                    raise NoViableAltException(self)

                self.state = 35
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 36
            self.match(pintlangParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ProcessContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(pintlangParser.ProcessContext, self).__init__(parent, invokingState)
            self.parser = parser
            self.id_dec = None # Token
            self.i = None # Token

        def PROC(self):
            return self.getToken(pintlangParser.PROC, 0)

        def IDENT(self):
            return self.getToken(pintlangParser.IDENT, 0)

        def INT(self):
            return self.getToken(pintlangParser.INT, 0)

        def getRuleIndex(self):
            return pintlangParser.RULE_process




    def process(self):

        localctx = pintlangParser.ProcessContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_process)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 38
            self.match(pintlangParser.PROC)
            self.state = 39
            localctx.id_dec = self.match(pintlangParser.IDENT)
            self.state = 40
            localctx.i = self.match(pintlangParser.INT)
            self.check_process((None if localctx.id_dec is None else localctx.id_dec.text), int((None if localctx.i is None else localctx.i.text)), (0 if localctx.id_dec is None else localctx.id_dec.line))
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ActionContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(pintlangParser.ActionContext, self).__init__(parent, invokingState)
            self.parser = parser
            self.fid = None # Token
            self.flev = None # Token
            self.cid = None # Token
            self.lev1 = None # Token
            self.lev2 = None # Token

        def ARROW(self):
            return self.getToken(pintlangParser.ARROW, 0)

        def IDENT(self, i=None):
            if i is None:
                return self.getTokens(pintlangParser.IDENT)
            else:
                return self.getToken(pintlangParser.IDENT, i)

        def INT(self, i=None):
            if i is None:
                return self.getTokens(pintlangParser.INT)
            else:
                return self.getToken(pintlangParser.INT, i)

        def getRuleIndex(self):
            return pintlangParser.RULE_action




    def action(self):

        localctx = pintlangParser.ActionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_action)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 43
            localctx.fid = self.match(pintlangParser.IDENT)
            self.state = 44
            localctx.flev = self.match(pintlangParser.INT)
            self.state = 45
            self.match(pintlangParser.ARROW)
            self.state = 46
            localctx.cid = self.match(pintlangParser.IDENT)
            self.state = 47
            localctx.lev1 = self.match(pintlangParser.INT)
            self.state = 48
            localctx.lev2 = self.match(pintlangParser.INT)
            self.check_action((None if localctx.fid is None else localctx.fid.text), int((None if localctx.flev is None else localctx.flev.text)), (None if localctx.cid is None else localctx.cid.text), int((None if localctx.lev1 is None else localctx.lev1.text)), int((None if localctx.lev2 is None else localctx.lev2.text)), (0 if localctx.fid is None else localctx.fid.line))
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class CooperContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(pintlangParser.CooperContext, self).__init__(parent, invokingState)
            self.parser = parser
            self.lha = None # Coop_lhsContext

        def COOP(self):
            return self.getToken(pintlangParser.COOP, 0)

        def LP(self):
            return self.getToken(pintlangParser.LP, 0)

        def COM(self):
            return self.getToken(pintlangParser.COM, 0)

        def coop_rhs(self):
            return self.getTypedRuleContext(pintlangParser.Coop_rhsContext,0)


        def RP(self):
            return self.getToken(pintlangParser.RP, 0)

        def coop_lhs(self):
            return self.getTypedRuleContext(pintlangParser.Coop_lhsContext,0)


        def getRuleIndex(self):
            return pintlangParser.RULE_cooper




    def cooper(self):

        localctx = pintlangParser.CooperContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_cooper)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 51
            self.match(pintlangParser.COOP)
            self.state = 52
            self.match(pintlangParser.LP)
            self.state = 53
            localctx.lha = self.coop_lhs()
            self.state = 54
            self.match(pintlangParser.COM)
            self.state = 55
            self.coop_rhs(localctx.lha.transition, localctx.lha.id_list, localctx.lha.condition)
            self.state = 56
            self.match(pintlangParser.RP)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Coop_lhsContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(pintlangParser.Coop_lhsContext, self).__init__(parent, invokingState)
            self.parser = parser
            self.transition = None
            self.id_list = None
            self.condition = None
            self.il = None # Sort_listContext
            self.rc = None # Rcoop_lhsContext

        def sort_list(self):
            return self.getTypedRuleContext(pintlangParser.Sort_listContext,0)


        def rcoop_lhs(self):
            return self.getTypedRuleContext(pintlangParser.Rcoop_lhsContext,0)


        def getRuleIndex(self):
            return pintlangParser.RULE_coop_lhs




    def coop_lhs(self):

        localctx = pintlangParser.Coop_lhsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_coop_lhs)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 58
            localctx.il = self.sort_list()
            localctx.id_list = localctx.il.id_list
            self.state = 60
            localctx.rc = self.rcoop_lhs(localctx.id_list)

            localctx.transition = localctx.rc.transition
            localctx.condition = localctx.rc.condition
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Rcoop_lhsContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1, ids=None):
            super(pintlangParser.Rcoop_lhsContext, self).__init__(parent, invokingState)
            self.parser = parser
            self.ids = None
            self.transition = None
            self.condition = None
            self.kw = None # Token
            self.tt = None # Tt_conditionContext
            self.cond = None # Tail_logexpContext
            self.id_dec = None # Token
            self.i1 = None # Token
            self.i2 = None # Token
            self.ids = ids

        def IN_KW(self):
            return self.getToken(pintlangParser.IN_KW, 0)

        def tt_condition(self):
            return self.getTypedRuleContext(pintlangParser.Tt_conditionContext,0)


        def tail_logexp(self):
            return self.getTypedRuleContext(pintlangParser.Tail_logexpContext,0)


        def ARROW(self):
            return self.getToken(pintlangParser.ARROW, 0)

        def IDENT(self):
            return self.getToken(pintlangParser.IDENT, 0)

        def INT(self, i=None):
            if i is None:
                return self.getTokens(pintlangParser.INT)
            else:
                return self.getToken(pintlangParser.INT, i)

        def getRuleIndex(self):
            return pintlangParser.RULE_rcoop_lhs




    def rcoop_lhs(self, ids):

        localctx = pintlangParser.Rcoop_lhsContext(self, self._ctx, self.state, ids)
        self.enterRule(localctx, 10, self.RULE_rcoop_lhs)
        try:
            self.state = 73
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [pintlangParser.IN_KW]:
                self.enterOuterAlt(localctx, 1)
                self.state = 63
                localctx.kw = self.match(pintlangParser.IN_KW)
                self.state = 64
                localctx.tt = self.tt_condition()
                self.state = 65
                localctx.cond = self.tail_logexp()
                localctx.condition = self.tt_translate_lcond(ids, localctx.tt.l_cond,  'line '+str((0 if localctx.kw is None else localctx.kw.line))+':') + localctx.cond.cond
                pass
            elif token in [pintlangParser.ARROW]:
                self.enterOuterAlt(localctx, 2)
                self.state = 68
                self.match(pintlangParser.ARROW)
                self.state = 69
                localctx.id_dec = self.match(pintlangParser.IDENT)
                self.state = 70
                localctx.i1 = self.match(pintlangParser.INT)
                self.state = 71
                localctx.i2 = self.match(pintlangParser.INT)
                localctx.transition = self.check_transition((None if localctx.id_dec is None else localctx.id_dec.text), int((None if localctx.i1 is None else localctx.i1.text)), int((None if localctx.i2 is None else localctx.i2.text)), (0 if localctx.id_dec is None else localctx.id_dec.line))
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Coop_rhsContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1, tr=None, il=None, cond=None):
            super(pintlangParser.Coop_rhsContext, self).__init__(parent, invokingState)
            self.parser = parser
            self.tr = None
            self.il = None
            self.cond = None
            self.tt = None # Tt_conditionContext
            self.id_dec = None # Token
            self.cc = None # Token
            self.i1 = None # Token
            self.i2 = None # Token
            self.tr = tr
            self.il = il
            self.cond = cond

        def tt_condition(self):
            return self.getTypedRuleContext(pintlangParser.Tt_conditionContext,0)


        def COM(self, i=None):
            if i is None:
                return self.getTokens(pintlangParser.COM)
            else:
                return self.getToken(pintlangParser.COM, i)

        def IDENT(self):
            return self.getToken(pintlangParser.IDENT, 0)

        def INT(self, i=None):
            if i is None:
                return self.getTokens(pintlangParser.INT)
            else:
                return self.getToken(pintlangParser.INT, i)

        def getRuleIndex(self):
            return pintlangParser.RULE_coop_rhs




    def coop_rhs(self, tr, il, cond):

        localctx = pintlangParser.Coop_rhsContext(self, self._ctx, self.state, tr, il, cond)
        self.enterRule(localctx, 12, self.RULE_coop_rhs)
        try:
            self.state = 84
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [pintlangParser.LB]:
                self.enterOuterAlt(localctx, 1)
                self.state = 75
                localctx.tt = self.tt_condition()
                self.check_tt_rhs(tr,il, localctx.tt.l_cond, )
                pass
            elif token in [pintlangParser.IDENT]:
                self.enterOuterAlt(localctx, 2)
                self.state = 78
                localctx.id_dec = self.match(pintlangParser.IDENT)
                self.state = 79
                localctx.cc = self.match(pintlangParser.COM)
                self.state = 80
                localctx.i1 = self.match(pintlangParser.INT)
                self.state = 81
                self.match(pintlangParser.COM)
                self.state = 82
                localctx.i2 = self.match(pintlangParser.INT)
                self.check_tr_rhs(cond, (None if localctx.id_dec is None else localctx.id_dec.text), int((None if localctx.i1 is None else localctx.i1.text)), int((None if localctx.i2 is None else localctx.i2.text)), (0 if localctx.cc is None else localctx.cc.line))
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Sort_listContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(pintlangParser.Sort_listContext, self).__init__(parent, invokingState)
            self.parser = parser
            self.id_list = None
            self.id1 = None # Token
            self.id2 = None # Token

        def LB(self):
            return self.getToken(pintlangParser.LB, 0)

        def RB(self):
            return self.getToken(pintlangParser.RB, 0)

        def IDENT(self, i=None):
            if i is None:
                return self.getTokens(pintlangParser.IDENT)
            else:
                return self.getToken(pintlangParser.IDENT, i)

        def SC(self, i=None):
            if i is None:
                return self.getTokens(pintlangParser.SC)
            else:
                return self.getToken(pintlangParser.SC, i)

        def getRuleIndex(self):
            return pintlangParser.RULE_sort_list




    def sort_list(self):

        localctx = pintlangParser.Sort_listContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_sort_list)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 86
            self.match(pintlangParser.LB)
            self.state = 87
            localctx.id1 = self.match(pintlangParser.IDENT)
            localctx.id_list = [(None if localctx.id1 is None else localctx.id1.text)]
            self.state = 94
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==pintlangParser.SC:
                self.state = 89
                self.match(pintlangParser.SC)
                self.state = 90
                localctx.id2 = self.match(pintlangParser.IDENT)
                localctx.id_list.append((None if localctx.id2 is None else localctx.id2.text))
                self.state = 96
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 97
            self.match(pintlangParser.RB)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Tt_conditionContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(pintlangParser.Tt_conditionContext, self).__init__(parent, invokingState)
            self.parser = parser
            self.l_cond = None
            self.tt1 = None # Tt_conjunctionContext
            self.tt2 = None # Tt_conjunctionContext

        def LB(self):
            return self.getToken(pintlangParser.LB, 0)

        def RB(self):
            return self.getToken(pintlangParser.RB, 0)

        def tt_conjunction(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(pintlangParser.Tt_conjunctionContext)
            else:
                return self.getTypedRuleContext(pintlangParser.Tt_conjunctionContext,i)


        def SC(self, i=None):
            if i is None:
                return self.getTokens(pintlangParser.SC)
            else:
                return self.getToken(pintlangParser.SC, i)

        def getRuleIndex(self):
            return pintlangParser.RULE_tt_condition




    def tt_condition(self):

        localctx = pintlangParser.Tt_conditionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_tt_condition)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 99
            self.match(pintlangParser.LB)
            self.state = 100
            localctx.tt1 = self.tt_conjunction()
            localctx.l_cond = [localctx.tt1.lval]
            self.state = 108
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==pintlangParser.SC:
                self.state = 102
                self.match(pintlangParser.SC)
                self.state = 103
                localctx.tt2 = self.tt_conjunction()
                localctx.l_cond.append(localctx.tt2.lval)
                self.state = 110
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 111
            self.match(pintlangParser.RB)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Tt_conjunctionContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(pintlangParser.Tt_conjunctionContext, self).__init__(parent, invokingState)
            self.parser = parser
            self.lval = None
            self.i1 = None # Token
            self.i2 = None # Token

        def LB(self):
            return self.getToken(pintlangParser.LB, 0)

        def RB(self):
            return self.getToken(pintlangParser.RB, 0)

        def INT(self, i=None):
            if i is None:
                return self.getTokens(pintlangParser.INT)
            else:
                return self.getToken(pintlangParser.INT, i)

        def SC(self, i=None):
            if i is None:
                return self.getTokens(pintlangParser.SC)
            else:
                return self.getToken(pintlangParser.SC, i)

        def getRuleIndex(self):
            return pintlangParser.RULE_tt_conjunction




    def tt_conjunction(self):

        localctx = pintlangParser.Tt_conjunctionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 18, self.RULE_tt_conjunction)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 113
            self.match(pintlangParser.LB)
            self.state = 114
            localctx.i1 = self.match(pintlangParser.INT)
            localctx.lval = [int((None if localctx.i1 is None else localctx.i1.text))]
            self.state = 121
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==pintlangParser.SC:
                self.state = 116
                self.match(pintlangParser.SC)
                self.state = 117
                localctx.i2 = self.match(pintlangParser.INT)
                localctx.lval.append(int((None if localctx.i2 is None else localctx.i2.text)))
                self.state = 123
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 124
            self.match(pintlangParser.RB)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Tail_logexpContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(pintlangParser.Tail_logexpContext, self).__init__(parent, invokingState)
            self.parser = parser
            self.cond = None
            self.tt1 = None # Tt_logexpContext
            self.tt2 = None # Tt_logexpContext

        def AND(self):
            return self.getToken(pintlangParser.AND, 0)

        def tt_logexp(self):
            return self.getTypedRuleContext(pintlangParser.Tt_logexpContext,0)


        def OR(self):
            return self.getToken(pintlangParser.OR, 0)

        def tt_primary(self):
            return self.getTypedRuleContext(pintlangParser.Tt_primaryContext,0)


        def getRuleIndex(self):
            return pintlangParser.RULE_tail_logexp




    def tail_logexp(self):

        localctx = pintlangParser.Tail_logexpContext(self, self._ctx, self.state)
        self.enterRule(localctx, 20, self.RULE_tail_logexp)
        try:
            self.state = 138
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [pintlangParser.COM]:
                self.enterOuterAlt(localctx, 1)
                localctx.cond = ""
                pass
            elif token in [pintlangParser.AND]:
                self.enterOuterAlt(localctx, 2)
                self.state = 127
                self.match(pintlangParser.AND)
                self.state = 128
                localctx.tt1 = self.tt_logexp()
                localctx.cond = ' and ' + localctx.tt1.cond
                pass
            elif token in [pintlangParser.OR]:
                self.enterOuterAlt(localctx, 3)
                self.state = 131
                self.match(pintlangParser.OR)
                self.state = 132
                localctx.tt2 = self.tt_logexp()
                localctx.cond = ' or ' + localctx.tt2.cond
                pass
            elif token in [pintlangParser.LP, pintlangParser.LB, pintlangParser.NOT]:
                self.enterOuterAlt(localctx, 4)
                self.state = 135
                self.tt_primary()
                localctx.cond = ""
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Tt_logexpContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(pintlangParser.Tt_logexpContext, self).__init__(parent, invokingState)
            self.parser = parser
            self.cond = None
            self.tt1 = None # Tt_logexp2Context
            self.tt2 = None # Tt_logexp2Context

        def tt_logexp2(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(pintlangParser.Tt_logexp2Context)
            else:
                return self.getTypedRuleContext(pintlangParser.Tt_logexp2Context,i)


        def OR(self, i=None):
            if i is None:
                return self.getTokens(pintlangParser.OR)
            else:
                return self.getToken(pintlangParser.OR, i)

        def getRuleIndex(self):
            return pintlangParser.RULE_tt_logexp




    def tt_logexp(self):

        localctx = pintlangParser.Tt_logexpContext(self, self._ctx, self.state)
        self.enterRule(localctx, 22, self.RULE_tt_logexp)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 140
            localctx.tt1 = self.tt_logexp2()
            localctx.cond = localctx.tt1.cond
            self.state = 148
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==pintlangParser.OR:
                self.state = 142
                self.match(pintlangParser.OR)
                self.state = 143
                localctx.tt2 = self.tt_logexp2()
                localctx.cond = localctx.cond + ' or ' + localctx.tt2.cond
                self.state = 150
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Tt_logexp2Context(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(pintlangParser.Tt_logexp2Context, self).__init__(parent, invokingState)
            self.parser = parser
            self.cond = None
            self.tt1 = None # Tt_primaryContext
            self.tt2 = None # Tt_primaryContext

        def tt_primary(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(pintlangParser.Tt_primaryContext)
            else:
                return self.getTypedRuleContext(pintlangParser.Tt_primaryContext,i)


        def AND(self, i=None):
            if i is None:
                return self.getTokens(pintlangParser.AND)
            else:
                return self.getToken(pintlangParser.AND, i)

        def getRuleIndex(self):
            return pintlangParser.RULE_tt_logexp2




    def tt_logexp2(self):

        localctx = pintlangParser.Tt_logexp2Context(self, self._ctx, self.state)
        self.enterRule(localctx, 24, self.RULE_tt_logexp2)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 151
            localctx.tt1 = self.tt_primary()
            localctx.cond = localctx.tt1.cond
            self.state = 159
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==pintlangParser.AND:
                self.state = 153
                self.match(pintlangParser.AND)
                self.state = 154
                localctx.tt2 = self.tt_primary()
                localctx.cond = localctx.cond + ' and ' + localctx.tt2.cond
                self.state = 161
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Tt_primaryContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(pintlangParser.Tt_primaryContext, self).__init__(parent, invokingState)
            self.parser = parser
            self.cond = None
            self.tt1 = None # Tt_primaryContext
            self.st = None # Sort_listContext
            self.kw = None # Token
            self.tt2 = None # Tt_conditionContext
            self.tt3 = None # Tt_logexpContext

        def NOT(self):
            return self.getToken(pintlangParser.NOT, 0)

        def tt_primary(self):
            return self.getTypedRuleContext(pintlangParser.Tt_primaryContext,0)


        def sort_list(self):
            return self.getTypedRuleContext(pintlangParser.Sort_listContext,0)


        def IN_KW(self):
            return self.getToken(pintlangParser.IN_KW, 0)

        def tt_condition(self):
            return self.getTypedRuleContext(pintlangParser.Tt_conditionContext,0)


        def LP(self):
            return self.getToken(pintlangParser.LP, 0)

        def RP(self):
            return self.getToken(pintlangParser.RP, 0)

        def tt_logexp(self):
            return self.getTypedRuleContext(pintlangParser.Tt_logexpContext,0)


        def getRuleIndex(self):
            return pintlangParser.RULE_tt_primary




    def tt_primary(self):

        localctx = pintlangParser.Tt_primaryContext(self, self._ctx, self.state)
        self.enterRule(localctx, 26, self.RULE_tt_primary)
        try:
            self.state = 176
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [pintlangParser.NOT]:
                self.enterOuterAlt(localctx, 1)
                self.state = 162
                self.match(pintlangParser.NOT)
                self.state = 163
                localctx.tt1 = self.tt_primary()
                localctx.cond = 'not (' + localctx.tt1.cond + ')'
                pass
            elif token in [pintlangParser.LB]:
                self.enterOuterAlt(localctx, 2)
                self.state = 166
                localctx.st = self.sort_list()
                self.state = 167
                localctx.kw = self.match(pintlangParser.IN_KW)
                self.state = 168
                localctx.tt2 = self.tt_condition()
                localctx.cond = self.tt_translate_lcond(localctx.st.id_list, localctx.tt2.l_cond, 'line '+str((0 if localctx.kw is None else localctx.kw.line))+':')
                pass
            elif token in [pintlangParser.LP]:
                self.enterOuterAlt(localctx, 3)
                self.state = 171
                self.match(pintlangParser.LP)
                self.state = 172
                localctx.tt3 = self.tt_logexp()
                self.state = 173
                self.match(pintlangParser.RP)
                localctx.cond = '('+localctx.tt3.cond+')'
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





