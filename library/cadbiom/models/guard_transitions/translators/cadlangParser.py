# Generated from cadbiom/models/guard_transitions/translators/cadlang.g by ANTLR 4.8
# encoding: utf-8
from __future__ import print_function
from antlr4 import *
from io import StringIO
import sys





def serializedATN():
    with StringIO() as buf:
        buf.write(u"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3")
        buf.write(u"\36\u0101\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7")
        buf.write(u"\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t")
        buf.write(u"\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22")
        buf.write(u"\3\2\3\2\3\2\3\2\5\2)\n\2\3\2\3\2\3\2\6\2.\n\2\r\2\16")
        buf.write(u"\2/\3\2\3\2\3\2\3\2\5\2\66\n\2\3\2\3\2\3\3\3\3\3\3\3")
        buf.write(u"\3\3\3\5\3?\n\3\3\3\3\3\3\3\3\4\3\4\5\4F\n\4\3\4\3\4")
        buf.write(u"\3\4\3\4\3\4\5\4M\n\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4")
        buf.write(u"\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\5\4a\n\4\3\4")
        buf.write(u"\5\4d\n\4\3\5\3\5\3\6\5\6i\n\6\3\6\3\6\3\6\3\6\5\6o\n")
        buf.write(u"\6\3\6\3\6\3\7\3\7\5\7u\n\7\3\b\3\b\3\b\3\b\3\b\3\b\7")
        buf.write(u"\b}\n\b\f\b\16\b\u0080\13\b\3\b\3\b\3\b\3\t\3\t\3\t\3")
        buf.write(u"\t\3\t\3\t\7\t\u008b\n\t\f\t\16\t\u008e\13\t\3\n\3\n")
        buf.write(u"\3\n\3\n\3\n\3\n\7\n\u0096\n\n\f\n\16\n\u0099\13\n\3")
        buf.write(u"\13\3\13\3\13\3\13\3\13\3\13\3\13\5\13\u00a2\n\13\3\f")
        buf.write(u"\3\f\3\f\3\f\3\f\3\f\7\f\u00aa\n\f\f\f\16\f\u00ad\13")
        buf.write(u"\f\3\f\5\f\u00b0\n\f\3\r\3\r\3\r\3\r\3\r\3\r\7\r\u00b8")
        buf.write(u"\n\r\f\r\16\r\u00bb\13\r\3\16\3\16\3\16\3\16\3\16\3\16")
        buf.write(u"\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\5\16\u00cb\n")
        buf.write(u"\16\3\17\3\17\3\17\3\17\5\17\u00d1\n\17\3\20\3\20\3\20")
        buf.write(u"\3\20\3\20\3\20\7\20\u00d9\n\20\f\20\16\20\u00dc\13\20")
        buf.write(u"\3\20\3\20\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3")
        buf.write(u"\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21")
        buf.write(u"\3\21\5\21\u00f4\n\21\3\22\3\22\3\22\3\22\3\22\3\22\7")
        buf.write(u"\22\u00fc\n\22\f\22\16\22\u00ff\13\22\3\22\2\2\23\2\4")
        buf.write(u"\6\b\n\f\16\20\22\24\26\30\32\34\36 \"\2\3\3\2\21\22")
        buf.write(u"\2\u010e\2$\3\2\2\2\49\3\2\2\2\6c\3\2\2\2\be\3\2\2\2")
        buf.write(u"\nh\3\2\2\2\ft\3\2\2\2\16v\3\2\2\2\20\u0084\3\2\2\2\22")
        buf.write(u"\u008f\3\2\2\2\24\u00a1\3\2\2\2\26\u00af\3\2\2\2\30\u00b1")
        buf.write(u"\3\2\2\2\32\u00ca\3\2\2\2\34\u00d0\3\2\2\2\36\u00d2\3")
        buf.write(u"\2\2\2 \u00f3\3\2\2\2\"\u00f5\3\2\2\2$(\b\2\1\2%&\7\25")
        buf.write(u"\2\2&\'\7\35\2\2\')\b\2\1\2(%\3\2\2\2()\3\2\2\2)-\3\2")
        buf.write(u"\2\2*.\5\16\b\2+.\5\6\4\2,.\5\4\3\2-*\3\2\2\2-+\3\2\2")
        buf.write(u"\2-,\3\2\2\2./\3\2\2\2/-\3\2\2\2/\60\3\2\2\2\60\61\3")
        buf.write(u"\2\2\2\61\65\b\2\1\2\62\63\5\36\20\2\63\64\b\2\1\2\64")
        buf.write(u"\66\3\2\2\2\65\62\3\2\2\2\65\66\3\2\2\2\66\67\3\2\2\2")
        buf.write(u"\678\7\2\2\38\3\3\2\2\29:\b\3\1\2:>\7\35\2\2;<\5\b\5")
        buf.write(u"\2<=\b\3\1\2=?\3\2\2\2>;\3\2\2\2>?\3\2\2\2?@\3\2\2\2")
        buf.write(u"@A\7\16\2\2AB\b\3\1\2B\5\3\2\2\2CE\7\35\2\2DF\5\b\5\2")
        buf.write(u"ED\3\2\2\2EF\3\2\2\2FG\3\2\2\2GH\7\20\2\2HI\7\35\2\2")
        buf.write(u"IJ\7\16\2\2JL\5\n\6\2KM\5\b\5\2LK\3\2\2\2LM\3\2\2\2M")
        buf.write(u"N\3\2\2\2NO\5\f\7\2OP\7\16\2\2PQ\b\4\1\2Qd\3\2\2\2RS")
        buf.write(u"\7\35\2\2ST\7\20\2\2TU\7\16\2\2UV\5\n\6\2VW\5\f\7\2W")
        buf.write(u"X\7\16\2\2XY\b\4\1\2Yd\3\2\2\2Z[\7\20\2\2[\\\7\35\2\2")
        buf.write(u"\\`\7\16\2\2]^\5\f\7\2^_\7\16\2\2_a\3\2\2\2`]\3\2\2\2")
        buf.write(u"`a\3\2\2\2ab\3\2\2\2bd\b\4\1\2cC\3\2\2\2cR\3\2\2\2cZ")
        buf.write(u"\3\2\2\2d\7\3\2\2\2ef\t\2\2\2f\t\3\2\2\2gi\5\20\t\2h")
        buf.write(u"g\3\2\2\2hi\3\2\2\2ij\3\2\2\2jk\7\f\2\2kl\5\26\f\2ln")
        buf.write(u"\7\r\2\2mo\7\35\2\2nm\3\2\2\2no\3\2\2\2op\3\2\2\2pq\b")
        buf.write(u"\6\1\2q\13\3\2\2\2ru\7\36\2\2su\3\2\2\2tr\3\2\2\2ts\3")
        buf.write(u"\2\2\2u\r\3\2\2\2vw\7\23\2\2wx\7\35\2\2x~\b\b\1\2y}\5")
        buf.write(u"\16\b\2z}\5\6\4\2{}\5\4\3\2|y\3\2\2\2|z\3\2\2\2|{\3\2")
        buf.write(u"\2\2}\u0080\3\2\2\2~|\3\2\2\2~\177\3\2\2\2\177\u0081")
        buf.write(u"\3\2\2\2\u0080~\3\2\2\2\u0081\u0082\7\24\2\2\u0082\u0083")
        buf.write(u"\b\b\1\2\u0083\17\3\2\2\2\u0084\u0085\5\22\n\2\u0085")
        buf.write(u"\u008c\b\t\1\2\u0086\u0087\7\26\2\2\u0087\u0088\5\22")
        buf.write(u"\n\2\u0088\u0089\b\t\1\2\u0089\u008b\3\2\2\2\u008a\u0086")
        buf.write(u"\3\2\2\2\u008b\u008e\3\2\2\2\u008c\u008a\3\2\2\2\u008c")
        buf.write(u"\u008d\3\2\2\2\u008d\21\3\2\2\2\u008e\u008c\3\2\2\2\u008f")
        buf.write(u"\u0090\5\24\13\2\u0090\u0097\b\n\1\2\u0091\u0092\7\27")
        buf.write(u"\2\2\u0092\u0093\5\26\f\2\u0093\u0094\b\n\1\2\u0094\u0096")
        buf.write(u"\3\2\2\2\u0095\u0091\3\2\2\2\u0096\u0099\3\2\2\2\u0097")
        buf.write(u"\u0095\3\2\2\2\u0097\u0098\3\2\2\2\u0098\23\3\2\2\2\u0099")
        buf.write(u"\u0097\3\2\2\2\u009a\u009b\7\35\2\2\u009b\u00a2\b\13")
        buf.write(u"\1\2\u009c\u009d\7\n\2\2\u009d\u009e\5\20\t\2\u009e\u009f")
        buf.write(u"\7\13\2\2\u009f\u00a0\b\13\1\2\u00a0\u00a2\3\2\2\2\u00a1")
        buf.write(u"\u009a\3\2\2\2\u00a1\u009c\3\2\2\2\u00a2\25\3\2\2\2\u00a3")
        buf.write(u"\u00a4\5\30\r\2\u00a4\u00ab\b\f\1\2\u00a5\u00a6\7\6\2")
        buf.write(u"\2\u00a6\u00a7\5\30\r\2\u00a7\u00a8\b\f\1\2\u00a8\u00aa")
        buf.write(u"\3\2\2\2\u00a9\u00a5\3\2\2\2\u00aa\u00ad\3\2\2\2\u00ab")
        buf.write(u"\u00a9\3\2\2\2\u00ab\u00ac\3\2\2\2\u00ac\u00b0\3\2\2")
        buf.write(u"\2\u00ad\u00ab\3\2\2\2\u00ae\u00b0\b\f\1\2\u00af\u00a3")
        buf.write(u"\3\2\2\2\u00af\u00ae\3\2\2\2\u00b0\27\3\2\2\2\u00b1\u00b2")
        buf.write(u"\5\32\16\2\u00b2\u00b9\b\r\1\2\u00b3\u00b4\7\5\2\2\u00b4")
        buf.write(u"\u00b5\5\32\16\2\u00b5\u00b6\b\r\1\2\u00b6\u00b8\3\2")
        buf.write(u"\2\2\u00b7\u00b3\3\2\2\2\u00b8\u00bb\3\2\2\2\u00b9\u00b7")
        buf.write(u"\3\2\2\2\u00b9\u00ba\3\2\2\2\u00ba\31\3\2\2\2\u00bb\u00b9")
        buf.write(u"\3\2\2\2\u00bc\u00bd\7\7\2\2\u00bd\u00be\5\32\16\2\u00be")
        buf.write(u"\u00bf\b\16\1\2\u00bf\u00cb\3\2\2\2\u00c0\u00c1\5\34")
        buf.write(u"\17\2\u00c1\u00c2\b\16\1\2\u00c2\u00cb\3\2\2\2\u00c3")
        buf.write(u"\u00c4\7\35\2\2\u00c4\u00cb\b\16\1\2\u00c5\u00c6\7\n")
        buf.write(u"\2\2\u00c6\u00c7\5\26\f\2\u00c7\u00c8\7\13\2\2\u00c8")
        buf.write(u"\u00c9\b\16\1\2\u00c9\u00cb\3\2\2\2\u00ca\u00bc\3\2\2")
        buf.write(u"\2\u00ca\u00c0\3\2\2\2\u00ca\u00c3\3\2\2\2\u00ca\u00c5")
        buf.write(u"\3\2\2\2\u00cb\33\3\2\2\2\u00cc\u00cd\7\b\2\2\u00cd\u00d1")
        buf.write(u"\b\17\1\2\u00ce\u00cf\7\t\2\2\u00cf\u00d1\b\17\1\2\u00d0")
        buf.write(u"\u00cc\3\2\2\2\u00d0\u00ce\3\2\2\2\u00d1\35\3\2\2\2\u00d2")
        buf.write(u"\u00d3\7\30\2\2\u00d3\u00d4\5 \21\2\u00d4\u00da\b\20")
        buf.write(u"\1\2\u00d5\u00d6\5 \21\2\u00d6\u00d7\b\20\1\2\u00d7\u00d9")
        buf.write(u"\3\2\2\2\u00d8\u00d5\3\2\2\2\u00d9\u00dc\3\2\2\2\u00da")
        buf.write(u"\u00d8\3\2\2\2\u00da\u00db\3\2\2\2\u00db\u00dd\3\2\2")
        buf.write(u"\2\u00dc\u00da\3\2\2\2\u00dd\u00de\7\31\2\2\u00de\37")
        buf.write(u"\3\2\2\2\u00df\u00e0\7\32\2\2\u00e0\u00e1\7\n\2\2\u00e1")
        buf.write(u"\u00e2\5\"\22\2\u00e2\u00e3\7\13\2\2\u00e3\u00e4\b\21")
        buf.write(u"\1\2\u00e4\u00f4\3\2\2\2\u00e5\u00e6\7\33\2\2\u00e6\u00e7")
        buf.write(u"\7\n\2\2\u00e7\u00e8\5\"\22\2\u00e8\u00e9\7\13\2\2\u00e9")
        buf.write(u"\u00ea\b\21\1\2\u00ea\u00f4\3\2\2\2\u00eb\u00ec\7\34")
        buf.write(u"\2\2\u00ec\u00ed\7\n\2\2\u00ed\u00ee\5\20\t\2\u00ee\u00ef")
        buf.write(u"\7\17\2\2\u00ef\u00f0\5\20\t\2\u00f0\u00f1\7\13\2\2\u00f1")
        buf.write(u"\u00f2\b\21\1\2\u00f2\u00f4\3\2\2\2\u00f3\u00df\3\2\2")
        buf.write(u"\2\u00f3\u00e5\3\2\2\2\u00f3\u00eb\3\2\2\2\u00f4!\3\2")
        buf.write(u"\2\2\u00f5\u00f6\5\20\t\2\u00f6\u00fd\b\22\1\2\u00f7")
        buf.write(u"\u00f8\7\17\2\2\u00f8\u00f9\5\20\t\2\u00f9\u00fa\b\22")
        buf.write(u"\1\2\u00fa\u00fc\3\2\2\2\u00fb\u00f7\3\2\2\2\u00fc\u00ff")
        buf.write(u"\3\2\2\2\u00fd\u00fb\3\2\2\2\u00fd\u00fe\3\2\2\2\u00fe")
        buf.write(u"#\3\2\2\2\u00ff\u00fd\3\2\2\2\33(-/\65>EL`chnt|~\u008c")
        buf.write(u"\u0097\u00a1\u00ab\u00af\u00b9\u00ca\u00d0\u00da\u00f3")
        buf.write(u"\u00fd")
        return buf.getvalue()


class cadlangParser ( Parser ):

    grammarFileName = "cadlang.g"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ u"<INVALID>", u"<INVALID>", u"<INVALID>", u"'and'", 
                     u"'or'", u"'not'", u"'true'", u"'false'", u"'('", u"')'", 
                     u"'['", u"']'", u"';'", u"','", u"'-->'", u"'/p'", 
                     u"'/i'", u"'/macro'", u"'/endmacro'", u"'/name'", u"'default'", 
                     u"'when'", u"'/constraints'", u"'/endconstraints'", 
                     u"'synchro'", u"'exclus'", u"'included'" ]

    symbolicNames = [ u"<INVALID>", u"WS", u"COMMENT", u"AND", u"OR", u"NOT", 
                      u"T", u"F", u"LP", u"RP", u"LB", u"RB", u"SC", u"COM", 
                      u"TARROW", u"PERM", u"INPUT", u"MACRO", u"ENDMACRO", 
                      u"NAME", u"DEFAULT", u"WHEN", u"CONST", u"ENDCONST", 
                      u"SYNC", u"EXCL", u"INC", u"IDENT", u"TEXT" ]

    RULE_cad_model = 0
    RULE_dec = 1
    RULE_transition = 2
    RULE_modifier = 3
    RULE_guard = 4
    RULE_note = 5
    RULE_macro = 6
    RULE_sig_expression = 7
    RULE_sig_exp = 8
    RULE_sig_exp_primary = 9
    RULE_bool_exp = 10
    RULE_bool_and = 11
    RULE_bool_primary = 12
    RULE_bool_constant = 13
    RULE_constraints = 14
    RULE_const_exp = 15
    RULE_exp_list = 16

    ruleNames =  [ u"cad_model", u"dec", u"transition", u"modifier", u"guard", 
                   u"note", u"macro", u"sig_expression", u"sig_exp", u"sig_exp_primary", 
                   u"bool_exp", u"bool_and", u"bool_primary", u"bool_constant", 
                   u"constraints", u"const_exp", u"exp_list" ]

    EOF = Token.EOF
    WS=1
    COMMENT=2
    AND=3
    OR=4
    NOT=5
    T=6
    F=7
    LP=8
    RP=9
    LB=10
    RB=11
    SC=12
    COM=13
    TARROW=14
    PERM=15
    INPUT=16
    MACRO=17
    ENDMACRO=18
    NAME=19
    DEFAULT=20
    WHEN=21
    CONST=22
    ENDCONST=23
    SYNC=24
    EXCL=25
    INC=26
    IDENT=27
    TEXT=28

    def __init__(self, input, output=sys.stdout):
        super(cadlangParser, self).__init__(input, output=output)
        self.checkVersion("4.8")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None



    def set_error_reporter(self, err):
        self.error_reporter = err

    def displayRecognitionError(self, tokenNames, re):
        hdr = self.getErrorHeader(re)
        msg = self.getErrorMessage(re, tokenNames)
        self.error_reporter.display(hdr + " " + msg)

    def displayExceptionMessage(self, e):
        msg = self.getErrorMessage(self, e, tokenNames)
        self.error_reporter.display(msg)

    def check_ident_dec(self, id, type, line):
        """
        @param id : string
        """
        line_txt = "line " + str(line) + ":"
        if id in self.symb_tab:
            self.error_reporter.display(line_txt + " Node double declaration: " + id)
        else:
            if type == "S":
                node = self.current_macro.add_simple_node(id, 0, 0)
            elif type == "P":
                node = self.current_macro.add_perm_node(id, 0, 0)
            elif type == "I":
                node = self.current_macro.add_input_node(id, 0, 0)
            else:
                self.error_reporter.display(line_txt + " Unknown type - bug?")
            self.symb_tab[id] = node

    def check_ident_trans(self, id, type, line):
        """
        @param id : string
        """
        line_txt = "line " + str(line) + ":"
        try:
            # already used node
            node = self.symb_tab[id]
            tok = (
                (node.is_input() and type == "I")
                or (node.is_perm() and type == "P")
                or (type == "N")
            )
            if not tok:
                self.error_reporter.display(
                    line_txt + " Type error in transition: " + id
                )
        except KeyError:
            if type == "N":
                node = self.current_macro.add_simple_node(id, 0, 0)
            elif type == "P":
                node = self.current_macro.add_perm_node(id, 0, 0)
            elif type == "I":
                node = self.current_macro.add_input_node(id, 0, 0)
            else:
                self.error_reporter.display(line_txt + " Unknown type - bug?")
            self.symb_tab[id] = node
        return node

    def check_ident_start(self, id, line):
        """
        @param id : string
        """
        line_txt = "line " + str(line) + ":"
        try:
            # already used node, must be simple or macro
            node = self.symb_tab[id]
            tnok = node.is_input() or node.is_perm()
            if tnok:
                self.error_reporter.display(
                    line_txt + " Type error in start transition: " + id
                )
        except KeyError:
            node = self.current_macro.add_simple_node(id, 0, 0)
            self.symb_tab[id] = node
        return node

    def check_ident_deg(self, id, line):
        """
        @param id : string
        """
        line_txt = "line " + str(line) + ":"
        try:
            # already used node, must be simple
            node = self.symb_tab[id]
            if not (node.is_simple() or node.is_macro()):
                # degradation allowed from macro nodes - TO MODIFY??
                self.error_reporter.display(
                    line_txt + " Type error in degradation transition: " + id
                )
        except KeyError:
            node = self.current_macro.add_simple_node(id, 0, 0)
            self.symb_tab[id] = node
        return node

    def build_transition(self, id1, modif, id2, modif2, gc, note, line):
        line_txt = "line " + str(line) + ":"
        if modif:
            type = self.modif_code(modif)
        else:
            type = "N"
        ori = self.check_ident_trans(id1, type, line)
        if modif2:
            type2 = self.modif_code(modif)
        else:
            type2 = "N"
        target = self.check_ident_trans(id2, type2, line)
        # check if origin and target are in the same macro
        if not ori.father == target.father:
            self.error_reporter.display(line_txt + " Node:" + id1 + " and node:" + id2 + " are not in the same macro")
        if gc[0]:
            event = gc[0]
        else:
            event = ""
        if gc[1]:
            cond = gc[1]
        else:
            cond = ""
        note_txt = self.clean_note(note)
        t = self.current_macro.add_transition(ori, target)
        if event:
            t.set_event(event)
        t.set_condition(cond)
        if len(note_txt) > 0:
            t.note = note_txt

    def build_start_transition(self, id, note, line):
        ori = self.current_macro.add_start_node(0, 0)
        target = self.check_ident_start(id, line)
        t = self.current_macro.add_transition(ori, target)
        if note:
            note_txt = self.clean_note(note)
            t.set_note(note_txt)

    def build_deg_transition(self, id, gc, note, line):
        target = self.current_macro.add_trap_node(0, 0)
        ori = self.check_ident_deg(id, line)
        event = gc[0]
        cond = gc[1]
        self.clean_note(note)
        t = self.current_macro.add_transition(ori, target)
        if event:
            t.set_event(event)
        t.set_condition(cond)

    def enter_macro(self, id, line):
        line_txt = "line " + str(line) + ":"
        self.macro_pile.append(self.current_macro)
        try:
            node = self.symb_tab[id]
            if not node.is_macro():
                self.error_reporter.display(
                    line_txt + " Not macro node used as macro:" + id
                )
                return
            else:
                self.error_reporter.display(line_txt + " Macro double definition:" + id)

        except KeyError:
            node = self.current_macro.add_macro_subnode(id, 0, 0, 0.25, 0.25)
            self.symb_tab[id] = node
        self.current_macro = node

    def leave_macro(self, id=None):
        self.current_macro = self.macro_pile.pop()

    def clean_note(self, note):
        return note[1:-1]

    def modif_code(self, modif):
        if modif == "/p":
            return "P"
        elif modif == "/i":
            return "I"

    def check_end(self):
        if len(self.macro_pile) > 0:
            self.error_reporter.display("Bad macro imbrication - missing \endmacro?")



    class Cad_modelContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1, model=None):
            super(cadlangParser.Cad_modelContext, self).__init__(parent, invokingState)
            self.parser = parser
            self.model = None
            self.id_dec = None # Token
            self.txt = None # ConstraintsContext
            self.model = model

        def EOF(self):
            return self.getToken(cadlangParser.EOF, 0)

        def NAME(self):
            return self.getToken(cadlangParser.NAME, 0)

        def macro(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(cadlangParser.MacroContext)
            else:
                return self.getTypedRuleContext(cadlangParser.MacroContext,i)


        def transition(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(cadlangParser.TransitionContext)
            else:
                return self.getTypedRuleContext(cadlangParser.TransitionContext,i)


        def dec(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(cadlangParser.DecContext)
            else:
                return self.getTypedRuleContext(cadlangParser.DecContext,i)


        def IDENT(self):
            return self.getToken(cadlangParser.IDENT, 0)

        def constraints(self):
            return self.getTypedRuleContext(cadlangParser.ConstraintsContext,0)


        def getRuleIndex(self):
            return cadlangParser.RULE_cad_model




    def cad_model(self, model):

        localctx = cadlangParser.Cad_modelContext(self, self._ctx, self.state, model)
        self.enterRule(localctx, 0, self.RULE_cad_model)

        self.model = model
        self.macro_pile = []
        self.symb_tab = dict()
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.current_macro = self.model.get_root()
            self.state = 38
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==cadlangParser.NAME:
                self.state = 35
                self.match(cadlangParser.NAME)
                self.state = 36
                localctx.id_dec = self.match(cadlangParser.IDENT)
                self.model.name = (None if localctx.id_dec is None else localctx.id_dec.text)


            self.state = 43 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 43
                self._errHandler.sync(self)
                la_ = self._interp.adaptivePredict(self._input,1,self._ctx)
                if la_ == 1:
                    self.state = 40
                    self.macro()
                    pass

                elif la_ == 2:
                    self.state = 41
                    self.transition()
                    pass

                elif la_ == 3:
                    self.state = 42
                    self.dec()
                    pass


                self.state = 45 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << cadlangParser.TARROW) | (1 << cadlangParser.MACRO) | (1 << cadlangParser.IDENT))) != 0)):
                    break

            self.check_end()
            self.state = 51
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==cadlangParser.CONST:
                self.state = 48
                localctx.txt = self.constraints()
                self.model.constraints = localctx.txt.text


            self.state = 53
            self.match(cadlangParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class DecContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(cadlangParser.DecContext, self).__init__(parent, invokingState)
            self.parser = parser
            self.id_dec = None # Token
            self.m = None # ModifierContext

        def SC(self):
            return self.getToken(cadlangParser.SC, 0)

        def IDENT(self):
            return self.getToken(cadlangParser.IDENT, 0)

        def modifier(self):
            return self.getTypedRuleContext(cadlangParser.ModifierContext,0)


        def getRuleIndex(self):
            return cadlangParser.RULE_dec




    def dec(self):

        localctx = cadlangParser.DecContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_dec)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            type = 'S'
            self.state = 56
            localctx.id_dec = self.match(cadlangParser.IDENT)
            self.state = 60
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==cadlangParser.PERM or _la==cadlangParser.INPUT:
                self.state = 57
                localctx.m = self.modifier()
                type = self.modif_code((None if localctx.m is None else self._input.getText(localctx.m.start,localctx.m.stop)))


            self.state = 62
            self.match(cadlangParser.SC)
            self.check_ident_dec((None if localctx.id_dec is None else localctx.id_dec.text), type, (0 if localctx.id_dec is None else localctx.id_dec.line))
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class TransitionContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(cadlangParser.TransitionContext, self).__init__(parent, invokingState)
            self.parser = parser
            self.id1 = None # Token
            self.m = None # ModifierContext
            self.id2 = None # Token
            self.gc = None # GuardContext
            self.m2 = None # ModifierContext
            self.ntext = None # NoteContext
            self.id3 = None # Token
            self.gc1 = None # GuardContext
            self.ntext1 = None # NoteContext
            self.id4 = None # Token
            self.ntext2 = None # NoteContext

        def TARROW(self):
            return self.getToken(cadlangParser.TARROW, 0)

        def SC(self, i=None):
            if i is None:
                return self.getTokens(cadlangParser.SC)
            else:
                return self.getToken(cadlangParser.SC, i)

        def IDENT(self, i=None):
            if i is None:
                return self.getTokens(cadlangParser.IDENT)
            else:
                return self.getToken(cadlangParser.IDENT, i)

        def guard(self):
            return self.getTypedRuleContext(cadlangParser.GuardContext,0)


        def note(self):
            return self.getTypedRuleContext(cadlangParser.NoteContext,0)


        def modifier(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(cadlangParser.ModifierContext)
            else:
                return self.getTypedRuleContext(cadlangParser.ModifierContext,i)


        def getRuleIndex(self):
            return cadlangParser.RULE_transition




    def transition(self):

        localctx = cadlangParser.TransitionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_transition)
        self._la = 0 # Token type
        try:
            self.state = 97
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,8,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 65
                localctx.id1 = self.match(cadlangParser.IDENT)
                self.state = 67
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==cadlangParser.PERM or _la==cadlangParser.INPUT:
                    self.state = 66
                    localctx.m = self.modifier()


                self.state = 69
                self.match(cadlangParser.TARROW)
                self.state = 70
                localctx.id2 = self.match(cadlangParser.IDENT)
                self.state = 71
                self.match(cadlangParser.SC)
                self.state = 72
                localctx.gc = self.guard()
                self.state = 74
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==cadlangParser.PERM or _la==cadlangParser.INPUT:
                    self.state = 73
                    localctx.m2 = self.modifier()


                self.state = 76
                localctx.ntext = self.note()
                self.state = 77
                self.match(cadlangParser.SC)
                self.build_transition((None if localctx.id1 is None else localctx.id1.text), (None if localctx.m is None else self._input.getText(localctx.m.start,localctx.m.stop)), (None if localctx.id2 is None else localctx.id2.text), (None if localctx.m2 is None else self._input.getText(localctx.m2.start,localctx.m2.stop)), localctx.gc.guard_component, (None if localctx.ntext is None else self._input.getText(localctx.ntext.start,localctx.ntext.stop)), (0 if localctx.id1 is None else localctx.id1.line))
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 80
                localctx.id3 = self.match(cadlangParser.IDENT)
                self.state = 81
                self.match(cadlangParser.TARROW)
                self.state = 82
                self.match(cadlangParser.SC)
                self.state = 83
                localctx.gc1 = self.guard()
                self.state = 84
                localctx.ntext1 = self.note()
                self.state = 85
                self.match(cadlangParser.SC)
                self.build_deg_transition((None if localctx.id3 is None else localctx.id3.text), localctx.gc1.guard_component, (None if localctx.ntext1 is None else self._input.getText(localctx.ntext1.start,localctx.ntext1.stop)), (0 if localctx.id3 is None else localctx.id3.line))
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 88
                self.match(cadlangParser.TARROW)
                self.state = 89
                localctx.id4 = self.match(cadlangParser.IDENT)
                self.state = 90
                self.match(cadlangParser.SC)
                self.state = 94
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==cadlangParser.SC or _la==cadlangParser.TEXT:
                    self.state = 91
                    localctx.ntext2 = self.note()
                    self.state = 92
                    self.match(cadlangParser.SC)


                self.build_start_transition((None if localctx.id4 is None else localctx.id4.text), (None if localctx.ntext2 is None else self._input.getText(localctx.ntext2.start,localctx.ntext2.stop)), (0 if localctx.id4 is None else localctx.id4.line))
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ModifierContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(cadlangParser.ModifierContext, self).__init__(parent, invokingState)
            self.parser = parser

        def PERM(self):
            return self.getToken(cadlangParser.PERM, 0)

        def INPUT(self):
            return self.getToken(cadlangParser.INPUT, 0)

        def getRuleIndex(self):
            return cadlangParser.RULE_modifier




    def modifier(self):

        localctx = cadlangParser.ModifierContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_modifier)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 99
            _la = self._input.LA(1)
            if not(_la==cadlangParser.PERM or _la==cadlangParser.INPUT):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class GuardContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(cadlangParser.GuardContext, self).__init__(parent, invokingState)
            self.parser = parser
            self.guard_component = None
            self.h = None # Sig_expressionContext
            self.bt = None # Bool_expContext
            self.id2 = None # Token

        def LB(self):
            return self.getToken(cadlangParser.LB, 0)

        def RB(self):
            return self.getToken(cadlangParser.RB, 0)

        def bool_exp(self):
            return self.getTypedRuleContext(cadlangParser.Bool_expContext,0)


        def sig_expression(self):
            return self.getTypedRuleContext(cadlangParser.Sig_expressionContext,0)


        def IDENT(self):
            return self.getToken(cadlangParser.IDENT, 0)

        def getRuleIndex(self):
            return cadlangParser.RULE_guard




    def guard(self):

        localctx = cadlangParser.GuardContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_guard)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 102
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==cadlangParser.LP or _la==cadlangParser.IDENT:
                self.state = 101
                localctx.h = self.sig_expression()


            self.state = 104
            self.match(cadlangParser.LB)

            self.state = 105
            localctx.bt = self.bool_exp()
            self.state = 106
            self.match(cadlangParser.RB)
            self.state = 108
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==cadlangParser.IDENT:
                self.state = 107
                localctx.id2 = self.match(cadlangParser.IDENT)



            if localctx.h and localctx.id2: localctx.guard_component = (localctx.h.text, localctx.bt.text, (None if localctx.id2 is None else localctx.id2.text))
            elif localctx.h: localctx.guard_component = (localctx.h.text, localctx.bt.text, None)
            elif localctx.id2: localctx.guard_component = (None, localctx.bt.text, (None if localctx.id2 is None else localctx.id2.text))
            else: localctx.guard_component = (None, localctx.bt.text, None)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class NoteContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(cadlangParser.NoteContext, self).__init__(parent, invokingState)
            self.parser = parser

        def TEXT(self):
            return self.getToken(cadlangParser.TEXT, 0)

        def getRuleIndex(self):
            return cadlangParser.RULE_note




    def note(self):

        localctx = cadlangParser.NoteContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_note)
        try:
            self.state = 114
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [cadlangParser.TEXT]:
                self.enterOuterAlt(localctx, 1)
                self.state = 112
                self.match(cadlangParser.TEXT)
                pass
            elif token in [cadlangParser.SC]:
                self.enterOuterAlt(localctx, 2)

                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class MacroContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(cadlangParser.MacroContext, self).__init__(parent, invokingState)
            self.parser = parser
            self.id1 = None # Token

        def MACRO(self):
            return self.getToken(cadlangParser.MACRO, 0)

        def ENDMACRO(self):
            return self.getToken(cadlangParser.ENDMACRO, 0)

        def IDENT(self):
            return self.getToken(cadlangParser.IDENT, 0)

        def macro(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(cadlangParser.MacroContext)
            else:
                return self.getTypedRuleContext(cadlangParser.MacroContext,i)


        def transition(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(cadlangParser.TransitionContext)
            else:
                return self.getTypedRuleContext(cadlangParser.TransitionContext,i)


        def dec(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(cadlangParser.DecContext)
            else:
                return self.getTypedRuleContext(cadlangParser.DecContext,i)


        def getRuleIndex(self):
            return cadlangParser.RULE_macro




    def macro(self):

        localctx = cadlangParser.MacroContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_macro)
        id2 = None
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 116
            self.match(cadlangParser.MACRO)
            self.state = 117
            localctx.id1 = self.match(cadlangParser.IDENT)
            self.enter_macro((None if localctx.id1 is None else localctx.id1.text), (0 if localctx.id1 is None else localctx.id1.line))
            self.state = 124
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << cadlangParser.TARROW) | (1 << cadlangParser.MACRO) | (1 << cadlangParser.IDENT))) != 0):
                self.state = 122
                self._errHandler.sync(self)
                la_ = self._interp.adaptivePredict(self._input,12,self._ctx)
                if la_ == 1:
                    self.state = 119
                    self.macro()
                    pass

                elif la_ == 2:
                    self.state = 120
                    self.transition()
                    pass

                elif la_ == 3:
                    self.state = 121
                    self.dec()
                    pass


                self.state = 126
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 127
            self.match(cadlangParser.ENDMACRO)
            self.leave_macro()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Sig_expressionContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(cadlangParser.Sig_expressionContext, self).__init__(parent, invokingState)
            self.parser = parser
            self.text = None
            self.exp1 = None # Sig_expContext
            self.exp2 = None # Sig_expContext

        def sig_exp(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(cadlangParser.Sig_expContext)
            else:
                return self.getTypedRuleContext(cadlangParser.Sig_expContext,i)


        def DEFAULT(self, i=None):
            if i is None:
                return self.getTokens(cadlangParser.DEFAULT)
            else:
                return self.getToken(cadlangParser.DEFAULT, i)

        def getRuleIndex(self):
            return cadlangParser.RULE_sig_expression




    def sig_expression(self):

        localctx = cadlangParser.Sig_expressionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_sig_expression)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 130
            localctx.exp1 = self.sig_exp()
            localctx.text = localctx.exp1.text
            self.state = 138
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==cadlangParser.DEFAULT:
                self.state = 132
                self.match(cadlangParser.DEFAULT)
                self.state = 133
                localctx.exp2 = self.sig_exp()
                localctx.text = localctx.text + ' default '+ localctx.exp2.text
                self.state = 140
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Sig_expContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(cadlangParser.Sig_expContext, self).__init__(parent, invokingState)
            self.parser = parser
            self.text = None
            self.exp1 = None # Sig_exp_primaryContext
            self.exp2 = None # Bool_expContext

        def sig_exp_primary(self):
            return self.getTypedRuleContext(cadlangParser.Sig_exp_primaryContext,0)


        def WHEN(self, i=None):
            if i is None:
                return self.getTokens(cadlangParser.WHEN)
            else:
                return self.getToken(cadlangParser.WHEN, i)

        def bool_exp(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(cadlangParser.Bool_expContext)
            else:
                return self.getTypedRuleContext(cadlangParser.Bool_expContext,i)


        def getRuleIndex(self):
            return cadlangParser.RULE_sig_exp




    def sig_exp(self):

        localctx = cadlangParser.Sig_expContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_sig_exp)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 141
            localctx.exp1 = self.sig_exp_primary()
            localctx.text = localctx.exp1.text
            self.state = 149
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==cadlangParser.WHEN:
                self.state = 143
                self.match(cadlangParser.WHEN)
                self.state = 144
                localctx.exp2 = self.bool_exp()
                localctx.text = localctx.text + ' when '+ localctx.exp2.text
                self.state = 151
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Sig_exp_primaryContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(cadlangParser.Sig_exp_primaryContext, self).__init__(parent, invokingState)
            self.parser = parser
            self.text = None
            self.id_dec = None # Token
            self.se = None # Sig_expressionContext

        def IDENT(self):
            return self.getToken(cadlangParser.IDENT, 0)

        def LP(self):
            return self.getToken(cadlangParser.LP, 0)

        def RP(self):
            return self.getToken(cadlangParser.RP, 0)

        def sig_expression(self):
            return self.getTypedRuleContext(cadlangParser.Sig_expressionContext,0)


        def getRuleIndex(self):
            return cadlangParser.RULE_sig_exp_primary




    def sig_exp_primary(self):

        localctx = cadlangParser.Sig_exp_primaryContext(self, self._ctx, self.state)
        self.enterRule(localctx, 18, self.RULE_sig_exp_primary)
        try:
            self.state = 159
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [cadlangParser.IDENT]:
                self.enterOuterAlt(localctx, 1)
                self.state = 152
                localctx.id_dec = self.match(cadlangParser.IDENT)
                localctx.text = (None if localctx.id_dec is None else localctx.id_dec.text)
                pass
            elif token in [cadlangParser.LP]:
                self.enterOuterAlt(localctx, 2)
                self.state = 154
                self.match(cadlangParser.LP)
                self.state = 155
                localctx.se = self.sig_expression()
                self.state = 156
                self.match(cadlangParser.RP)
                localctx.text = '('+localctx.se.text+')'
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Bool_expContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(cadlangParser.Bool_expContext, self).__init__(parent, invokingState)
            self.parser = parser
            self.text = None
            self.b1 = None # Bool_andContext
            self.b2 = None # Bool_andContext

        def bool_and(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(cadlangParser.Bool_andContext)
            else:
                return self.getTypedRuleContext(cadlangParser.Bool_andContext,i)


        def OR(self, i=None):
            if i is None:
                return self.getTokens(cadlangParser.OR)
            else:
                return self.getToken(cadlangParser.OR, i)

        def getRuleIndex(self):
            return cadlangParser.RULE_bool_exp




    def bool_exp(self):

        localctx = cadlangParser.Bool_expContext(self, self._ctx, self.state)
        self.enterRule(localctx, 20, self.RULE_bool_exp)
        self._la = 0 # Token type
        try:
            self.state = 173
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [cadlangParser.NOT, cadlangParser.T, cadlangParser.F, cadlangParser.LP, cadlangParser.IDENT]:
                self.enterOuterAlt(localctx, 1)
                self.state = 161
                localctx.b1 = self.bool_and()
                localctx.text = localctx.b1.text
                self.state = 169
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==cadlangParser.OR:
                    self.state = 163
                    self.match(cadlangParser.OR)
                    self.state = 164
                    localctx.b2 = self.bool_and()
                    localctx.text = localctx.text + ' '+'or '+localctx.b2.text
                    self.state = 171
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                pass
            elif token in [cadlangParser.RP, cadlangParser.LB, cadlangParser.RB, cadlangParser.COM, cadlangParser.DEFAULT, cadlangParser.WHEN]:
                self.enterOuterAlt(localctx, 2)
                localctx.text = ""
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Bool_andContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(cadlangParser.Bool_andContext, self).__init__(parent, invokingState)
            self.parser = parser
            self.text = None
            self.b1 = None # Bool_primaryContext
            self.b2 = None # Bool_primaryContext

        def bool_primary(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(cadlangParser.Bool_primaryContext)
            else:
                return self.getTypedRuleContext(cadlangParser.Bool_primaryContext,i)


        def AND(self, i=None):
            if i is None:
                return self.getTokens(cadlangParser.AND)
            else:
                return self.getToken(cadlangParser.AND, i)

        def getRuleIndex(self):
            return cadlangParser.RULE_bool_and




    def bool_and(self):

        localctx = cadlangParser.Bool_andContext(self, self._ctx, self.state)
        self.enterRule(localctx, 22, self.RULE_bool_and)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 175
            localctx.b1 = self.bool_primary()
            localctx.text = localctx.b1.text
            self.state = 183
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==cadlangParser.AND:
                self.state = 177
                self.match(cadlangParser.AND)
                self.state = 178
                localctx.b2 = self.bool_primary()
                localctx.text = localctx.text + ' '+'and '+localctx.b2.text
                self.state = 185
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Bool_primaryContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(cadlangParser.Bool_primaryContext, self).__init__(parent, invokingState)
            self.parser = parser
            self.text = None
            self.b1 = None # Bool_primaryContext
            self.b2 = None # Bool_constantContext
            self.id_dec = None # Token
            self.b3 = None # Bool_expContext

        def NOT(self):
            return self.getToken(cadlangParser.NOT, 0)

        def bool_primary(self):
            return self.getTypedRuleContext(cadlangParser.Bool_primaryContext,0)


        def bool_constant(self):
            return self.getTypedRuleContext(cadlangParser.Bool_constantContext,0)


        def IDENT(self):
            return self.getToken(cadlangParser.IDENT, 0)

        def LP(self):
            return self.getToken(cadlangParser.LP, 0)

        def RP(self):
            return self.getToken(cadlangParser.RP, 0)

        def bool_exp(self):
            return self.getTypedRuleContext(cadlangParser.Bool_expContext,0)


        def getRuleIndex(self):
            return cadlangParser.RULE_bool_primary




    def bool_primary(self):

        localctx = cadlangParser.Bool_primaryContext(self, self._ctx, self.state)
        self.enterRule(localctx, 24, self.RULE_bool_primary)
        try:
            self.state = 200
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [cadlangParser.NOT]:
                self.enterOuterAlt(localctx, 1)
                self.state = 186
                self.match(cadlangParser.NOT)
                self.state = 187
                localctx.b1 = self.bool_primary()
                localctx.text = 'not '+localctx.b1.text
                pass
            elif token in [cadlangParser.T, cadlangParser.F]:
                self.enterOuterAlt(localctx, 2)
                self.state = 190
                localctx.b2 = self.bool_constant()
                localctx.text = localctx.b2.text
                pass
            elif token in [cadlangParser.IDENT]:
                self.enterOuterAlt(localctx, 3)
                self.state = 193
                localctx.id_dec = self.match(cadlangParser.IDENT)
                localctx.text = (None if localctx.id_dec is None else localctx.id_dec.text)
                pass
            elif token in [cadlangParser.LP]:
                self.enterOuterAlt(localctx, 4)
                self.state = 195
                self.match(cadlangParser.LP)
                self.state = 196
                localctx.b3 = self.bool_exp()
                self.state = 197
                self.match(cadlangParser.RP)
                localctx.text = '('+localctx.b3.text+')'
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Bool_constantContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(cadlangParser.Bool_constantContext, self).__init__(parent, invokingState)
            self.parser = parser
            self.text = None

        def T(self):
            return self.getToken(cadlangParser.T, 0)

        def F(self):
            return self.getToken(cadlangParser.F, 0)

        def getRuleIndex(self):
            return cadlangParser.RULE_bool_constant




    def bool_constant(self):

        localctx = cadlangParser.Bool_constantContext(self, self._ctx, self.state)
        self.enterRule(localctx, 26, self.RULE_bool_constant)
        try:
            self.state = 206
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [cadlangParser.T]:
                self.enterOuterAlt(localctx, 1)
                self.state = 202
                self.match(cadlangParser.T)
                localctx.text = 'true'
                pass
            elif token in [cadlangParser.F]:
                self.enterOuterAlt(localctx, 2)
                self.state = 204
                self.match(cadlangParser.F)
                localctx.text = 'false'
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ConstraintsContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(cadlangParser.ConstraintsContext, self).__init__(parent, invokingState)
            self.parser = parser
            self.text = None
            self.t1 = None # Const_expContext
            self.t2 = None # Const_expContext

        def CONST(self):
            return self.getToken(cadlangParser.CONST, 0)

        def ENDCONST(self):
            return self.getToken(cadlangParser.ENDCONST, 0)

        def const_exp(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(cadlangParser.Const_expContext)
            else:
                return self.getTypedRuleContext(cadlangParser.Const_expContext,i)


        def getRuleIndex(self):
            return cadlangParser.RULE_constraints




    def constraints(self):

        localctx = cadlangParser.ConstraintsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 28, self.RULE_constraints)
        SEP = ';'
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 208
            self.match(cadlangParser.CONST)
            self.state = 209
            localctx.t1 = self.const_exp()
            localctx.text = localctx.t1.text+SEP
            self.state = 216
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << cadlangParser.SYNC) | (1 << cadlangParser.EXCL) | (1 << cadlangParser.INC))) != 0):
                self.state = 211
                localctx.t2 = self.const_exp()
                localctx.text = localctx.text+'\n'+localctx.t2.text+SEP
                self.state = 218
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 219
            self.match(cadlangParser.ENDCONST)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Const_expContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(cadlangParser.Const_expContext, self).__init__(parent, invokingState)
            self.parser = parser
            self.text = None
            self.t1 = None # Exp_listContext
            self.t2 = None # Exp_listContext
            self.t3 = None # Sig_expressionContext
            self.t4 = None # Sig_expressionContext

        def SYNC(self):
            return self.getToken(cadlangParser.SYNC, 0)

        def LP(self):
            return self.getToken(cadlangParser.LP, 0)

        def RP(self):
            return self.getToken(cadlangParser.RP, 0)

        def exp_list(self):
            return self.getTypedRuleContext(cadlangParser.Exp_listContext,0)


        def EXCL(self):
            return self.getToken(cadlangParser.EXCL, 0)

        def INC(self):
            return self.getToken(cadlangParser.INC, 0)

        def COM(self):
            return self.getToken(cadlangParser.COM, 0)

        def sig_expression(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(cadlangParser.Sig_expressionContext)
            else:
                return self.getTypedRuleContext(cadlangParser.Sig_expressionContext,i)


        def getRuleIndex(self):
            return cadlangParser.RULE_const_exp




    def const_exp(self):

        localctx = cadlangParser.Const_expContext(self, self._ctx, self.state)
        self.enterRule(localctx, 30, self.RULE_const_exp)
        try:
            self.state = 241
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [cadlangParser.SYNC]:
                self.enterOuterAlt(localctx, 1)
                self.state = 221
                self.match(cadlangParser.SYNC)
                self.state = 222
                self.match(cadlangParser.LP)
                self.state = 223
                localctx.t1 = self.exp_list()
                self.state = 224
                self.match(cadlangParser.RP)
                localctx.text = 'synchro('+localctx.t1.text+')'
                pass
            elif token in [cadlangParser.EXCL]:
                self.enterOuterAlt(localctx, 2)
                self.state = 227
                self.match(cadlangParser.EXCL)
                self.state = 228
                self.match(cadlangParser.LP)
                self.state = 229
                localctx.t2 = self.exp_list()
                self.state = 230
                self.match(cadlangParser.RP)
                localctx.text = 'exclus('+localctx.t2.text+')'
                pass
            elif token in [cadlangParser.INC]:
                self.enterOuterAlt(localctx, 3)
                self.state = 233
                self.match(cadlangParser.INC)
                self.state = 234
                self.match(cadlangParser.LP)
                self.state = 235
                localctx.t3 = self.sig_expression()
                self.state = 236
                self.match(cadlangParser.COM)
                self.state = 237
                localctx.t4 = self.sig_expression()
                self.state = 238
                self.match(cadlangParser.RP)
                localctx.text = 'included('+localctx.t3.text+', '+ localctx.t4.text+')'
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Exp_listContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(cadlangParser.Exp_listContext, self).__init__(parent, invokingState)
            self.parser = parser
            self.text = None
            self.t1 = None # Sig_expressionContext
            self.t2 = None # Sig_expressionContext

        def sig_expression(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(cadlangParser.Sig_expressionContext)
            else:
                return self.getTypedRuleContext(cadlangParser.Sig_expressionContext,i)


        def COM(self, i=None):
            if i is None:
                return self.getTokens(cadlangParser.COM)
            else:
                return self.getToken(cadlangParser.COM, i)

        def getRuleIndex(self):
            return cadlangParser.RULE_exp_list




    def exp_list(self):

        localctx = cadlangParser.Exp_listContext(self, self._ctx, self.state)
        self.enterRule(localctx, 32, self.RULE_exp_list)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 243
            localctx.t1 = self.sig_expression()
            localctx.text = localctx.t1.text
            self.state = 251
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==cadlangParser.COM:
                self.state = 245
                self.match(cadlangParser.COM)
                self.state = 246
                localctx.t2 = self.sig_expression()
                localctx.text = localctx.text +', '+localctx.t2.text
                self.state = 253
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





