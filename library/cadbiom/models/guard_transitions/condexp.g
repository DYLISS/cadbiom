// Collect ident names in a boolean expression
//
// To generate a parser based on this grammar you'll need ANTLRv4, which you can
// get from 'https://www.antlr.org/download.html'.

grammar condexp;

options {
  language= Python;
  }
@header{
}
// RULES
// returns the local context with with idents attribut containing the set of ids in boolean expression
sig_bool
      @init{$ctx.idents = set()}
        : id1=sig_bool1 DOL {$ctx.idents = $ctx.id1.idents}
        | DOL 
        ;        
               
sig_bool1
      @init{$ctx.idents = set()}
        : id1=sig_bool_and {$ctx.idents = $ctx.id1.idents}
          (OR id2=sig_bool_and {$ctx.idents |= $ctx.id2.idents}
          )*
        ;
        
sig_bool_and
      @init{$ctx.idents = set()}
        : id1=sig_primary {$ctx.idents = $ctx.id1.idents}
          (AND id2=sig_primary {$ctx.idents |= $ctx.id2.idents}
          )*
        ;
        
sig_primary
      @init{$ctx.idents = set()}
        : NOT id1=sig_primary
            {$ctx.idents = $ctx.id1.idents}
            
        | id4=sig_constant
            {$ctx.idents = $ctx.id4.idents}
   
        | id2=IDENT
             {$ctx.idents = set([$id2.text.encode("utf8")])}
             
        | PG id3= sig_bool1 PD
             {$ctx.idents = $ctx.id3.idents}
        ;

sig_constant
      @init{$ctx.idents = set()}
         : T {$ctx.idents = set()}
         | F {$ctx.idents = set()}
         ;
         
//lexer
WS          :  (' '|'\t'|'\n') -> channel(HIDDEN);

COMMENT     : '//'(~'\n')*'\n' -> channel(HIDDEN);

// keywords
AND     : 'and' ;
OR      : 'or' ;
NOT     : 'not';
T       : 'true';
F       : 'false';

PG      : '(' ;
PD      : ')' ;
DOL     : '$';


fragment LETTER   : 'a'..'z'|'A'..'Z'|'_';

fragment DIGIT    : '0'..'9';

IDENT   : ( LETTER|DIGIT )+ ;

         
