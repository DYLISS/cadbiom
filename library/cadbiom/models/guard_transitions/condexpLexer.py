# Generated from cadbiom/models/guard_transitions/condexp.g by ANTLR 4.8
# encoding: utf-8
from __future__ import print_function
from antlr4 import *
from io import StringIO
import sys





def serializedATN():
    with StringIO() as buf:
        buf.write(u"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2")
        buf.write(u"\rT\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t")
        buf.write(u"\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r")
        buf.write(u"\4\16\t\16\3\2\3\2\3\2\3\2\3\3\3\3\3\3\3\3\7\3&\n\3\f")
        buf.write(u"\3\16\3)\13\3\3\3\3\3\3\3\3\3\3\4\3\4\3\4\3\4\3\5\3\5")
        buf.write(u"\3\5\3\6\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\b\3\b\3\b")
        buf.write(u"\3\b\3\b\3\b\3\t\3\t\3\n\3\n\3\13\3\13\3\f\3\f\3\r\3")
        buf.write(u"\r\3\16\3\16\6\16Q\n\16\r\16\16\16R\2\2\17\3\3\5\4\7")
        buf.write(u"\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\2\31\2\33\r\3")
        buf.write(u"\2\5\4\2\13\f\"\"\3\2\f\f\5\2C\\aac|\2T\2\3\3\2\2\2\2")
        buf.write(u"\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3")
        buf.write(u"\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3")
        buf.write(u"\2\2\2\2\33\3\2\2\2\3\35\3\2\2\2\5!\3\2\2\2\7.\3\2\2")
        buf.write(u"\2\t\62\3\2\2\2\13\65\3\2\2\2\r9\3\2\2\2\17>\3\2\2\2")
        buf.write(u"\21D\3\2\2\2\23F\3\2\2\2\25H\3\2\2\2\27J\3\2\2\2\31L")
        buf.write(u"\3\2\2\2\33P\3\2\2\2\35\36\t\2\2\2\36\37\3\2\2\2\37 ")
        buf.write(u"\b\2\2\2 \4\3\2\2\2!\"\7\61\2\2\"#\7\61\2\2#\'\3\2\2")
        buf.write(u"\2$&\n\3\2\2%$\3\2\2\2&)\3\2\2\2\'%\3\2\2\2\'(\3\2\2")
        buf.write(u"\2(*\3\2\2\2)\'\3\2\2\2*+\7\f\2\2+,\3\2\2\2,-\b\3\2\2")
        buf.write(u"-\6\3\2\2\2./\7c\2\2/\60\7p\2\2\60\61\7f\2\2\61\b\3\2")
        buf.write(u"\2\2\62\63\7q\2\2\63\64\7t\2\2\64\n\3\2\2\2\65\66\7p")
        buf.write(u"\2\2\66\67\7q\2\2\678\7v\2\28\f\3\2\2\29:\7v\2\2:;\7")
        buf.write(u"t\2\2;<\7w\2\2<=\7g\2\2=\16\3\2\2\2>?\7h\2\2?@\7c\2\2")
        buf.write(u"@A\7n\2\2AB\7u\2\2BC\7g\2\2C\20\3\2\2\2DE\7*\2\2E\22")
        buf.write(u"\3\2\2\2FG\7+\2\2G\24\3\2\2\2HI\7&\2\2I\26\3\2\2\2JK")
        buf.write(u"\t\4\2\2K\30\3\2\2\2LM\4\62;\2M\32\3\2\2\2NQ\5\27\f\2")
        buf.write(u"OQ\5\31\r\2PN\3\2\2\2PO\3\2\2\2QR\3\2\2\2RP\3\2\2\2R")
        buf.write(u"S\3\2\2\2S\34\3\2\2\2\6\2\'PR\3\2\3\2")
        return buf.getvalue()


class condexpLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    WS = 1
    COMMENT = 2
    AND = 3
    OR = 4
    NOT = 5
    T = 6
    F = 7
    PG = 8
    PD = 9
    DOL = 10
    IDENT = 11

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ u"DEFAULT_MODE" ]

    literalNames = [ u"<INVALID>",
            u"'and'", u"'or'", u"'not'", u"'true'", u"'false'", u"'('", 
            u"')'", u"'$'" ]

    symbolicNames = [ u"<INVALID>",
            u"WS", u"COMMENT", u"AND", u"OR", u"NOT", u"T", u"F", u"PG", 
            u"PD", u"DOL", u"IDENT" ]

    ruleNames = [ u"WS", u"COMMENT", u"AND", u"OR", u"NOT", u"T", u"F", 
                  u"PG", u"PD", u"DOL", u"LETTER", u"DIGIT", u"IDENT" ]

    grammarFileName = u"condexp.g"

    def __init__(self, input=None, output=sys.stdout):
        super(condexpLexer, self).__init__(input, output=output)
        self.checkVersion("4.8")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


