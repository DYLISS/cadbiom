#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2016-2020  IRISA
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
# The original code contained here was initially developed by:
#
#     Pierre Vignet.
#     IRISA/IRSET
#     Dyliss team
#     IRISA Campus de Beaulieu
#     35042 RENNES Cedex, FRANCE

"""Definition of setup function for setuptools module."""

# Standard imports
from distutils import sysconfig
from setuptools import setup, Extension

__PACKAGE_VERSION__ = "0.3"
__LIBRARY_VERSION__ = "1.1.0"


# Delete unwanted flags for C compilation
# Distutils has the lovely feature of providing all the same flags that
# Python was compiled with. The result is that adding extra flags is easy,
# but removing them is a total pain. Doing so involves subclassing the
# compiler class, catching the arguments and manually removing the offending
# flag from the argument list used by the compile function.
# That's the theory anyway, the docs are too poor to actually guide you
# through what you have to do to make that happen.
d = sysconfig.get_config_vars()
for k, v in d.items():
    for unwanted in (' -g ',):
        if str(v).find(unwanted) != -1:
            v = d[k] = str(v).replace(unwanted, ' ')

modules = [
    Extension(
        "_cadbiom",
        ["_cadbiom/cadbiom.c"],
        language = "c",
        include_dirs=['_cadbiom', '.'],
        define_macros=[
            ('CADBIOM_VERSION', '"' + __LIBRARY_VERSION__ + '"'),
        ],
        extra_compile_args=[
            "-flto",
            #"-march=native",
            #"-mtune=native",
            "-Ofast",
            #"-Wall",
            #"-g", # Not define NDEBUG macro => Debug build
            "-DNDEBUG", # Force release build
            "-std=c11",
            "-Wno-unused-variable",
            "-Wno-unused-but-set-variable",
            # assume that signed arithmetic overflow of addition, subtraction
            # and multiplication wraps around using twos-complement
            # representation
            "-fwrapv",
            # BOF protect (use both)
            #"-D_FORTIFY_SOURCE=2",
            #"-fstack-protector-strong",
            "-pthread",
            "-DUSE_PTHREADS",
        ],
        extra_link_args=[
            "-fPIC",
            "-Ofast",
            #"-march=native",
            "-flto",
            "-std=c11",
            "-Wl,--as-needed",
        ]
    ),
]

setup(
    version=__PACKAGE_VERSION__,
    ext_modules=modules,
    description="Cadbiom library v%s" % __LIBRARY_VERSION__,
)
