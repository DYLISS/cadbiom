# -*- coding: utf-8 -*-
"""Unit tests for Chart model objects"""

from __future__ import unicode_literals
from __future__ import print_function

# Standard imports
import pytest

# lib imports
from cadbiom.models.guard_transitions.chart_model import CTransition, CSimpleNode
from cadbiom.models.biosignal.translators.gt_visitors import get_conditions_from_event

@pytest.yield_fixture()
def feed_transitions():
    """Build a CTransition object

    Event and condition are empty and they are intended to be specified and tested.
    """
    # def __init__(self, xcoord, ycoord, name, model, **kwargs)
    X_node = CSimpleNode(0.0, 0.0, "X", None)
    Y_node = CSimpleNode(0.0, 0.0, "Y", None)

    # def __init__(self, origin, extremity):
    transition = CTransition(X_node, Y_node)

    return transition


def test_get_influencing_places(feed_transitions):
    """Test function that retrieve all the places that influence a transition

    .. seealso:: :meth:`cadbiom.models.guard_transitions.chart_model.CTransition.get_influencing_places`

    Test condition alone:

    {"A", "B", "C"}:
    "A and B and not C" => {"A", "B", "C"}
    "A and B or not (C)" => {"A", "B", "C"}

    Test events alone:
    {"A", "B", "C", "D"}:
    ((_h_0) when (A and B)) default ((_h_1) when (C and D)) default (_h_2)
    ((_h_0) when (A and B)) default (((_h_1) when (C and D)) default (_h_2))


    Test events + condition
    It is not a condition to be found in real conditions.
    These 2 attributes are normally exclusive.
    /!\ Maybe it should raise an exception in a future update

    "C and D" + "((_h_0) when (A and B))"
    {"A", "B", "C", "D"}

    """
    trans = feed_transitions

    ## Test condition alone
    trans.condition = "A and B and not C"
    infl_places = trans.get_influencing_places()
    print(infl_places)
    expected = {"A", "B", "C"}
    assert infl_places == expected


    trans.condition = "A and B or not (C)"
    infl_places = trans.get_influencing_places()
    print(infl_places)
    assert infl_places == expected


    ## Test events alone
    trans.condition = ""
    trans.event = "_h_0"
    infl_places = trans.get_influencing_places()
    print(infl_places)
    expected = set()
    assert infl_places == expected

    # Default as an operator in a chain of defaults
    trans.event = "((_h_0) when (A and B)) default ((_h_1) when (C and D)) default (_h_2)"
    infl_places = trans.get_influencing_places()
    print(infl_places)
    expected = {"A", "B", "C", "D"}
    assert infl_places == expected

    # Default links only expressions 2 by 2
    trans.event = "((_h_0) when (A and B)) default (((_h_1) when (C and D)) default (_h_2))"
    infl_places = trans.get_influencing_places()
    print(infl_places)
    assert infl_places == expected


    ## Test events + condition
    ## It is not a condition to be found in real conditions.
    ## These 2 attributes are normally exclusive.
    ## /!\ Maybe it should raise an exception in a future update
    trans.condition = "C and D"
    trans.event = "((_h_0) when (A and B))"
    infl_places = trans.get_influencing_places()
    print(infl_places)
    expected = {"A", "B", "C", "D"}
    assert infl_places == expected


def test_get_conditions_from_event():
    """Test high level function to get conditions attached to events from
    a complex event description such as those encountered in transitions
    shared by many events.

    This function is used by
    :meth:`cadbiom.models.guard_transitions.chart_model.CTransition.get_influencing_places`

    .. seealso:: For low level tests of expressions
        :meth:`cadbiom.models.biosignal.translators.TestExprCompiler`
    """

    # Exception is expected
    # Simple binary expression (SigSyncBinExpr) is not supported in events description
    with pytest.raises(
        AssertionError, match=r".*The expression type.*"
    ):
        found = get_conditions_from_event("A and B and not C")
        print(found)


    found = get_conditions_from_event("_h_0")
    print(found)
    expected = {'_h_0': ''}
    assert found == expected


    found = get_conditions_from_event("((_h_0) when (A and B)) default ((_h_1) when (C and D)) default (_h_2)")
    print(found)
    expected = {'_h_0': '(A and B)', '_h_1': '(C and D)', '_h_2': ''}
    assert found == expected
