CADBIOM (Computer Aided Design of Biological Models) is an open source modelling software.
Based on Guarded transition semantic, it gives a formal framework to help the modelling of
biological systems such as cell signaling network.


# Installation

See the official website: [link](http://cadbiom.genouest.org/download.html).

# License

CADBIOM is freely available on cadbiom.genouest.org,
and distributed under the terms of the GNU General Public License.
